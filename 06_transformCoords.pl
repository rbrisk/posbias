#!/usr/bin/env perl

use strict;
use warnings;
use autodie;
use Getopt::Long;
use Pod::Usage;
use Data::Dumper;
$Data::Dumper::Sortkeys = 1;

my $man = 0;
my $help = 0;
my $delim = "\t";
my $minContigLen = 0;
my $pathPosMap;
my ($pathIn, $pathOut, $pathMap, $pathContigLen);

GetOptions(
      "h|help" => \$help,
      "man"    => \$man,
      "i=s"    => \$pathIn,
      "o=s"    => \$pathOut,
		"m=s"    => \$pathMap,
		"c=s"    => \$pathContigLen,
		"t=i"    => \$minContigLen,
		"pm=s"   => \$pathPosMap,
   ) or pod2usage(2);

pod2usage(1) if $help;
pod2usage(-verbose => 2) if $man;

die("Input file is required") unless $pathIn;
die("Map file is required") unless $pathMap;

sub mkMapRec {
	my ($header, $valueStr) = @_;
	chomp($valueStr);
	my @values = split($delim, $valueStr);
	my %rec;
	for my $i (0..$#$header) {
		$rec{$header->[$i]} = $values[$i];
	}
	return \%rec;
}

sub readMap {
	my ($pathMap) = @_;
	my %scafMaps;
	open(my $fhMap, '<', $pathMap);
	my $headerStr = <$fhMap>;
	chomp($headerStr);
	my @header = split($delim, $headerStr);
	while (<$fhMap>) {
		my $mapRec = mkMapRec(\@header, $_);
		my $scaf = $mapRec->{'Scaffold'};
		$scafMaps{$scaf} = [] unless defined $scafMaps{$scaf};
		push(@{$scafMaps{$scaf}}, $mapRec);
	}
	$fhMap->close();
	return \%scafMaps;
}

sub readContigLen {
	my ($path) = @_;
	my %contigLengths;
	open(my $fh, '<', $path);
	while (<$fh>) {
		chomp;
		my ($seqId, $len) = split($delim);
		$contigLengths{$seqId} = $len;
	}
	close($fh);
	return \%contigLengths;
}

	
sub transformCoord {
	my ($scafPos, $scafBeg, $contigBeg, $contigEnd, $strand) = @_;
	my $offset = $scafPos - $scafBeg;
	my $contigPos = $strand eq '+' ? $contigBeg + $offset : $contigEnd - $offset;
	return $contigPos;
}

sub isLongEnough {
	my ($name, $lenMap, $thres) = @_;
	my $isLong;
	if ($thres > 0) {
		$isLong = defined($lenMap->{$name}) && $lenMap->{$name} >= $thres;
	} else {
		$isLong = 1;
	}
	return $isLong;
}

# Check if the length map is required
my $contigLengths;
if ($minContigLen > 0) {
	die("Path to the file listing contig lengths is required when -t option is specified")
			unless $pathContigLen;
	$contigLengths = readContigLen($pathContigLen);
}

# Allpaths has a bad habbit of extending scaffolds past last contig. The extension is not reported
# in the final.summary file (surprise!). Therefore, we cannot traverse the map file at the same time
# as the input table. If there is a SNP in the extended region, the loop will run to end of the file
# because it would not be able to find the mapping. Thus, we have to read the map first and have a
# separate loop for each scaffold.
my $scafMaps = readMap($pathMap);

open(my $fhIn, '<', $pathIn);
my $fhOut;
if ($pathOut) {
	open($fhOut, '>', $pathOut);
} else {
	$fhOut = *STDOUT;
}


my $headerIn = <$fhIn>;
chomp($headerIn);
print $fhOut join($delim, $headerIn), "\n";

my $mapRec = { 'Scaffold' => ''};
my ($map, $i, @posMap);
while (<$fhIn>) {
	chomp;
	my @F = split($delim, $_);
	my ($scafId, $scafPos) = @F[0..1];
	if ($mapRec->{'Scaffold'} ne $scafId) {
		next unless defined($scafMaps->{$scafId});
		$map = $scafMaps->{$scafId};
		$i = 0;
		$mapRec = $map->[$i];
	}
	while ($i < $#$map && ($mapRec->{'SBeg'} > $scafPos || $mapRec->{'SEnd'} < $scafPos)) {
		$mapRec = $map->[++$i];
	}
	# Skip the record if it is not on a contig and if it is beyond the last contig end
	next unless $mapRec->{'CBeg'} && $mapRec->{'SEnd'} >= $scafPos;
	if ( isLongEnough($mapRec->{"Contig"}, $contigLengths, $minContigLen) ) {
		my $posContig1 = transformCoord(
				$scafPos, 
				$mapRec->{"SBeg"}, 
				$mapRec->{"CBeg"}, 
				$mapRec->{"CEnd"}, 
				$mapRec->{"CStrand"},
			);
		push(@posMap, join($delim, $mapRec->{"Contig"}, $posContig1, $F[0], $F[1]));
		$F[0] = $mapRec->{"Contig"};
		$F[1] = $posContig1;
		print $fhOut join($delim, @F), "\n";
	}
	if ($mapRec->{"Contig2"} && isLongEnough($mapRec->{"Contig2"}, $contigLengths, $minContigLen)) {
		my $posContig2 = transformCoord(
				$scafPos,
				$mapRec->{"SBeg"},
				$mapRec->{"CBeg2"},
				$mapRec->{"CEnd2"},
				$mapRec->{"CStrand2"},
			);
		push(@posMap, join($delim, $mapRec->{"Contig2"}, $posContig2, $F[0], $F[1]));
		$F[0] = $mapRec->{"Contig2"};
		$F[1] = $posContig2;
		print $fhOut join($delim, @F), "\n";
	}
}
$fhIn->close();
$fhOut->close();

if ($pathPosMap) {
	open(my $fhPosMap, '>', $pathPosMap);
	print $fhPosMap join($delim, qw/ Contig ContigPos Scaffold ScaffoldPos /), "\n";
	for my $row (@posMap) {
		print $fhPosMap "$row\n";
	}
	$fhPosMap->close();
}


__END__

=encoding utf8

=head1 NAME

06_transformCoords.pl - transforms scaffold coordinates into contig coordinates.

=head1 SYNOPSIS

06_transformCoords.pl -i variants.txt -o output.txt -m map.txt

=head1 OPTIONS

=over 8

=item B<-i>

Variant table extracted from a VCF file.

=item B<-m>

Map file from scaffold coordinates to contig coordinates. It should have the following columns:
Scaffold, SBeg, SEnd, Contig, CBeg, CEnd, CStrand, Contig2, CBeg2, CEnd2, CStrand2. Only + and -
values are allowed in the strand fields.

=item B<-o>

(Optional) Output file. It will be same as the input file but with transformed coordinates. Records
with coordinates that map to multiple contigs will be duplicated. Default: STDOUT

=item B<-c>

(Optional) File that lists contig lengths. Required if C<-t> option is specified.

=item B<-t>

(Optional) Minimum contig lengths. If specified, the script will ignore variants that map to contigs
that are shorter than the specified value. This option should be specified in conjunction with
C<-c>.

=item B<--pm>

(Optional) Path for printing the map of contig coordinates to scaffold coordinates. This map will
have the same number of lines as the output file. The reason to have this separately is because some
downstream scripts would fail if two extra columns are included in the output file.

=back

=head1 DESCRIPTION

The script converts scaffold coordinates to contig coordinates using the specified map file. If a
variant position can be mapped to multiple contigs, the variant record is duplicated for each
possible mapping. It is assumed that a region can only map to 0 to 2 contigs. The map file should
have no gaps in scaffold coordinates, i.e. all regions are continuous. Scaffolds should have the
same order as the variant file and all scaffolds should be present.

Optionally, you can instruct the script to ignore variants that are shorter than a certain
threshold. This can be accomplished by specifying C<-t> (threshold) and C<-c> (path to the file with
contig lengths) options.

=head1 AUTHOR

Roman Briskine, Universität Zürich

=cut

