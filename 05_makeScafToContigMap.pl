#!/usr/bin/env perl

use strict;
use warnings;
use autodie;
use Bio::Range;
use Bio::SeqIO;
use Carp;
use Getopt::Long;
use Pod::Usage;
use Set::Scalar;
use Data::Dumper;

my $man = 0;
my $help = 0;
my $scafTempl = "";
my %scafTemplates = (
		"allp" => "scaffold_%d",
		"soap" => "scaffold%d",
	);
my ($pathIn, $pathContigs, $pathOut, $fileType, $pathContigNameMap);

GetOptions(
      "h|help" => \$help,
      "man"    => \$man,
      "i=s"    => \$pathIn,
		"c=s"    => \$pathContigs,
      "o=s"    => \$pathOut,
      "t=s"    => \$fileType,
		"m=s"    => \$pathContigNameMap,
		"scaf-templ=s" => \$scafTempl,
   ) or pod2usage(2);

pod2usage(1) if $help;
pod2usage(-verbose => 2) if $man;

die("Input file is required") unless $pathIn;
die("Contig fasta file is required") unless $pathContigs;


package Record;

sub new {
	my ($class, %args) = @_;
	my $beg = $args{'-scafBeg'};
	my $len = $args{'-scafLen'};
	my $self = {
		-scafRange => Bio::Range->new(-start => $beg, -length => $len),
		-contigIds => [ ],
		-contigRanges => [ ],
	};
	bless $self, $class;
	return $self;
}

sub addContig {
	my ($self, %args) = @_;
	my $id = $args{'-id'};
	my $beg = $args{'-beg'};
	my $len = $args{'-len'};
	my $strand = $args{'-strand'};
	my $range = Bio::Range->new(-start => $beg, -length => $len, -strand => $strand);
	push(@{$self->{'-contigIds'}}, $id);
	push(@{$self->{'-contigRanges'}}, $range);
}

sub removeOverlap {
	my ($self, $overlapLen) = @_;
	$self->{'-scafRange'}->end($self->scafEnd - $overlapLen);
	if ($self->contigStrand < 0) {
		$self->{'-contigRanges'}[-1]->start($overlapLen + 1);
	} else {
		$self->{'-contigRanges'}[-1]->end($self->contigEnd - $overlapLen);
	}
}

sub scafEnd {
	my ($self) = @_;
	return $self->{'-scafRange'}->end;
}

sub contigId {
	my ($self) = @_;
	return $self->{'-contigIds'}[-1];
}

sub contigEnd {
	my ($self) = @_;
	return $self->{'-contigRanges'}[-1]->end;
}

sub contigStrand {
	my ($self) = @_;
	return $self->{'-contigRanges'}[-1]->strand;
}

sub toArrayRef {
	my ($self, $contigNameMap) = @_;
	my @out;
	my $scafRange = $self->{'-scafRange'};
	my $contigIds = $self->{'-contigIds'};
	my $contigRanges = $self->{'-contigRanges'};
	push(@out, $scafRange->start, $scafRange->end);
	for my $i (0..1) {
		if ($i < scalar(@$contigIds)) {
			my $range = $contigRanges->[$i];
			my $strand = $range->strand < 0 ? '-' : '+';
			my $cid = $contigIds->[$i];
			if ($contigNameMap && defined $contigNameMap->{$cid}) {
				$cid = $contigNameMap->{$cid};
			}
			push(@out, $cid, $range->start, $range->end, $strand);
		} else {
			push(@out, ("") x 4);
		}
	}
	return \@out;
}


package main;

sub readContigLengths {
	my ($path, $ptrn) = @_;
	my $reader = Bio::SeqIO->new(-file => $path, -format => 'fasta', -alphabet => 'dna');

	my %contigLen;
	while (my $seq = $reader->next_seq()) {
		(my $seqId = $seq->display_id) =~ s/$ptrn/$1/;
		$contigLen{$seqId} = $seq->length;
	}

	return \%contigLen;
}

sub getContigLen {
	my ($lengths, $id) = @_;
	my $contigLen = $lengths->{$id};
	croak("Contig '$id' was not found in the fasta file and " .
			"its length could not be determined") unless $contigLen;
	return $contigLen;
}

sub readContigNameMap {
	my ($path) = @_;
	open(my $fh, "<", $path);
	# Skip header
	<$fh>;
	my %map;
	while (<$fh>) {
		chomp;
		my ($origId, $newId) = split("\t", $_);
		$map{$origId} = $newId;
	}
	$fh->close();
	return \%map;
}

sub parseStrand {
	my ($strand) = @_;
	my $out = 0;
	if ($strand) {
		if ($strand =~ /^\+/ || $strand =~ /^1$/) {
			$out = 1;
		} elsif ($strand =~ /^-/) {
			$out = -1;
		}
	}
	return $out;
}

sub addFirstContig {
	my %args = @_;
	my $scafMap = $args{'-scafMap'};
	my $contigId = $args{'-contigId'};
	my $contigLen = $args{'-contigLen'};
	my $strand = parseStrand($args{'-strand'});

	my $rec = Record->new(-scafBeg => 1, -scafLen => $contigLen);
	$rec->addContig(-id => $contigId, -beg => 1, -len => $contigLen, -strand => $strand);
	push(@$scafMap, $rec);
}	

sub addGap {
	my %args = @_;
	my $scafMap = $args{'-scafMap'};
	my $gapLen = $args{'-gapLen'};
	my $prevRec = $scafMap->[-1];
	my $gapRec = Record->new(-scafBeg => $prevRec->scafEnd + 1, -scafLen => $gapLen);
	push(@$scafMap, $gapRec);
}

sub processOverlap {
	my %args = @_;
	my $scafMap = $args{'-scafMap'};
	my $overlapLen = $args{'-overlapLen'};
	my $contigId = $args{'-contigId'};
	my $contigLen = $args{'-contigLen'};
	my $strand = $args{'-strand'};

	my $prevRec = $scafMap->[-1];
	my $prevContig = defined $prevRec->{'-strand'} ? $prevRec : $scafMap->[-2];

	$prevContig->removeOverlap($overlapLen);
	my $overlapRec = Record->new(-scafBeg => $prevContig->scafEnd + 1, -scafLen => $overlapLen);
	$overlapRec->addContig(
			-id  => $prevContig->contigId, 
			-beg => $prevContig->contigStrand < 0 ? 1 : $prevContig->contigEnd + 1, 
			-len => $overlapLen,
			-strand => $prevContig->contigStrand,
		);
	$overlapRec->addContig(
			-id  => $contigId,
			-beg => $strand < 0 ? $contigLen - $overlapLen + 1 : 1,
			-len => $overlapLen,
			-strand => $strand,
		);
	push(@$scafMap, $overlapRec);
}

sub addSubseqContig {
	my %args = @_;
	my $scafMap = $args{'-scafMap'};
	my $gapLen = $args{'-gapLen'};
	my $contigId = $args{'-contigId'};
	my $contigLen = $args{'-contigLen'};
	my $extensionLen = $args{'-extensionLen'};
	my $strand = parseStrand($args{'-strand'});

	# Overlap and gap may exist at the same time (e.g. in SOAP). That's why the overlap is always
	# processed first.
	my $overlapLen = $contigLen - $extensionLen;
	if ($overlapLen > 0) {
		my $prevRec = $scafMap->[-1];

		$prevRec->removeOverlap($overlapLen);
		my $overlapRec = Record->new(-scafBeg => $prevRec->scafEnd + 1, -scafLen => $overlapLen);
		$overlapRec->addContig(
				-id  => $prevRec->contigId, 
				-beg => $prevRec->contigStrand < 0 ? 1 : $prevRec->contigEnd + 1, 
				-len => $overlapLen,
				-strand => $prevRec->contigStrand,
			);
		$overlapRec->addContig(
				-id  => $contigId,
				-beg => $strand < 0 ? $contigLen - $overlapLen + 1 : 1,
				-len => $overlapLen,
				-strand => $strand,
			);
		push(@$scafMap, $overlapRec);
	}

	if ($gapLen > 0) {
		my $prevRec = $scafMap->[-1];
		my $gapRec = Record->new(-scafBeg => $prevRec->scafEnd + 1, -scafLen => $gapLen);
		push(@$scafMap, $gapRec);
	}

	my $prevRec = $scafMap->[-1];
	my $rec = Record->new(
			-scafBeg => $prevRec->scafEnd + 1, 
			-scafLen => $extensionLen,
		);
	$rec->addContig(
			-id => $contigId, 
			-beg => $strand < 0 ? 1 : 1 + $overlapLen,
			-len => $extensionLen,
			-strand => $strand,
		);
	push(@$scafMap, $rec);
}

sub parseAllp {
	my ($fh, $pathContigs) = @_;

	my $contigLengths = readContigLengths($pathContigs, '^contig_(\d+)$');

	# Skip the comment on top. We assume it is a single paragraph separated from the data by a blank line.
	while (<$fh>) {
		last if $_ =~ /^\s*$/;
	}

	my %scaffolds;
	my $scafId = -1;
	my $scafMap;
	while (<$fh>) {
		chomp;
		if ($_ =~ /^\s*$/) {
			next;
		} elsif ($_ =~ /^scaffold (\d+)/) {
			# Rotate scaffold info
			$scafId = $1;
			$scaffolds{$scafId} = [];
			$scafMap = $scaffolds{$scafId};
		} elsif ($_ =~ /^(\d+).*\(l = (\d+)\)$/) {
			# Firt contig in the scaffold
			my $contigId  = $1;
			# For allpaths, contigLen and extensionLen should be the same but see the comment for SOAP
			my $contigLen = getContigLen($contigLengths, $contigId);
			my $extensionLen = $2;
			croak("Contig length '$contigLen' does not match extension length '$extensionLen' " .
					"for the first contig in scaffold '$scafId'") unless $contigLen == $extensionLen;
			addFirstContig(
					-scafMap => $scafMap,
					-contigId => $contigId,
					-contigLen => $contigLen,
				);
		} elsif ($_ =~ /^ -- \((-?\d+).+ --> (\d+) \(l = (\d+)\)$/) {
			# Subsequent contigs; negative gap means overlap
			my $gapLen    = $1;
			my $contigId  = $2;
			my $contigLen = getContigLen($contigLengths, $contigId);
			my $extensionLen = $gapLen < 0 ? $3 + $gapLen : $3;
			addSubseqContig(
					-scafMap => $scafMap, 
					-contigId => $contigId, 
					-contigLen => $contigLen, 
					-gapLen => $gapLen,
					-extensionLen => $extensionLen,
				);
		}
	}
	return \%scaffolds;
}


sub parseSoap {
	my ($fh, $pathContigs) = @_;

	my $contigLengths = readContigLengths($pathContigs, '^(\d+)$');
	
	my %scaffolds;
	my $scafId = -1;
	my $scafMap;
	while (<$fh>) {
		chomp;
		if ($_ =~ /^>scaffold(\d+)/) {
			# Rotate scaffold info
			$scafId = $1;
			$scaffolds{$scafId} = [];
			$scafMap = $scaffolds{$scafId};
		} else {
			# SOAPdenovo may insert a gap in the middle of a contig when two contigs overlap.
			# Therefore, we cannot use the fourth value as a proxi for contig length. We will call it
			# extensionLen as this is how far the scaffold is extended beyond the gap.
			my ($contigId, $scafBeg, $strand, $extensionLen) = split(/\s+/, $_);
			my $contigLen = getContigLen($contigLengths, $contigId);
			# scafBeg has origin 0 in the file. Let's make it origin 1.
			++$scafBeg;
			if ($scafBeg == 1) {
				# First contig in the scaffold
				croak("Contig length '$contigLen' does not match extension length '$extensionLen' " .
						"for the first contig in scaffold '$scafId'") unless $contigLen == $extensionLen;
				addFirstContig(
						-scafMap => $scafMap,
						-contigId => $contigId,
						-contigLen => $contigLen,
						-strand => $strand,
					);
			} else {
				my $prevRec = $scafMap->[-1];
				my $gapLen = $scafBeg - $prevRec->scafEnd - 1;
				addSubseqContig(
						-scafMap => $scafMap,
						-contigId => $contigId,
						-contigLen => $contigLen,
						-gapLen => $gapLen,
						-extensionLen => $extensionLen,
						-strand => $strand,
					);
			}
		}
	}
	return \%scaffolds;
}


my %dispatcher = ( 
		allp => \&parseAllp, 
		soap => \&parseSoap,
	);

die("Unsupported file type. Supported types: " . join(", ", keys %dispatcher)) 
		unless defined $dispatcher{$fileType};

open(my $fh, "<", $pathIn);
my $scaffolds = $dispatcher{$fileType}->($fh, $pathContigs);
$fh->close();


my $contigNameMap;
if ($pathContigNameMap) {
	$contigNameMap = readContigNameMap($pathContigNameMap);
}

		
my $fhOut;
if ($pathOut) {
	open($fhOut, "> ", $pathOut);
} else {
	$fhOut = \*STDOUT;
}
my @header = qw/ Scaffold SBeg SEnd Contig CBeg CEnd CStrand Contig2 CBeg2 CEnd2 CStrand2 /;
print $fhOut join("\t", @header), "\n";
$scafTempl = $scafTemplates{$fileType} unless $scafTempl;
for my $scafId (sort { $a <=> $b } keys(%$scaffolds)) {
	my $scafName = sprintf($scafTempl, $scafId);
	for my $rec ( @{$scaffolds->{$scafId}} ) {
		my $out = $rec->toArrayRef($contigNameMap);
		unshift(@$out, $scafName);
		print $fhOut join("\t", @$out), "\n";
	}
}
close($fhOut);


__END__

=encoding utf8

=head1 NAME

05_makeScafToContigMap.pl - Creates a map of scaffold coordinates to contig coordinates.

=head1 SYNOPSIS

05_makeScafToContigMap.pl -i input.txt -t allpaths -o positions.txt

=head1 OPTIONS

=over 8

=item B<-i>

Input file that describes contig positions within scaffolds.

=item B<-c>

Fasta file that contains contig sequences. The file is used to determine contig lengths.

=item B<-t>

Input type format. Currently supported formats: allp (allpaths), soap (soapdenovo)

=item B<--scaf-templ>

(Optional) Template for writing scaffold names. Default: "scaffold_%d" for allp and "scaffold%d" for
soap

=item B<-o>

(Optional) Output file. Tab-delimited file that includes scaffold id and position columns. Default:
STDOUT

=item B<-m>

(Optional) Path to the contig id map file. If specified, the contig names will be translated in the
output.

=back

=head1 DESCRIPTION

The script maps scaffold coordinates to contig coordinates. The script operates on the assumption
that at most two contigs can overlap. (Three overlapping contigs would entail homologous sequences
that would have been merged into two contigs when the graph is traversed.) Therefore, the output
contains a list of position ranges mapped to zero, one, or two contigs.

SOAPdenovo2 sometimes inserts a gap even when contigs overlap. Therefore, we have to know both
contig length and the length after the gap. Then, we will be able to remove overlap and create a
correct mapping. In fact, it would be beneficial to load contig lengths for other assemblers as a
sanity check.

=head1 AUTHOR

Roman Briskine, Universität Zürich

=cut
