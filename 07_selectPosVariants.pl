#!/usr/bin/env perl

use strict;
use warnings;
use Getopt::Long;
use Pod::Usage;

my $man = 0;
my $help = 0;
my $r = 0;
my ($pathVcf, $pathOut, $p);

GetOptions(
		"h|help" => \$help,
		"man"    => \$man,
		"i=s"    => \$pathVcf,
		"o=s"    => \$pathOut,
		"p=i"    => \$p,
		"r=i"    => \$r,
	) or pod2usage(2);

pod2usage(1) if $help;
pod2usage(-verbose => 2) if $man;

die("Input file is required") unless $pathVcf;
die("Position is required") unless $p;
die("Position '$p' should be positive") unless $p > 0;
$r = $p unless $r; 
die("Position '$r' should be positive") unless $r > 0;
die("Invalid position range '$p .. $r'") unless $p <= $r;


open(FIN, "<", $pathVcf) or die("Unable to read the input file ($pathVcf). $!");
open(FOUT, "> " . ($pathOut || '-')) or die("Unable to write the output ($pathOut). $!");

my %contigLen;
my $cnt = 0;
my $cntOut = 0;
while (<FIN>) {
	chomp;
	if ($_ =~ /^#/) {
		if ($_ =~ /^##contig=<ID=([^,]+),length=(\d+)>$/i) {
			$contigLen{$1} = $2;
		}
		next;
	}
	$cnt++;
	my ($seqId, $pos, $rest) = split("\t", $_, 3);
	die("Sequence length for ($seqId) is not available") unless defined($contigLen{$seqId});
	my $pos3P = $contigLen{$seqId} - $pos + 1;
	if (($pos >= $p && $pos <= $r) || ($pos3P >= $p && $pos3P <= $r)) {
		print FOUT $_, "\n";
		$cntOut++;
	}
}
close(FOUT);
close(FIN);

print STDERR "Selected $cntOut polymorphisms out of $cnt\n";


__END__

=encoding utf8

=head1 NAME

07_selectPosVariants.pl - Select all variants that start at the specified position(s).

=head1 SYNOPSIS

07_selectPosVariants.pl -i input.vcf -o variants.txt -p 72

=head1 OPTIONS

=over 8

=item B<-i>

Input file in VCF format. The file should contain sequence length information in its header.

=item B<-o>

(Optional) Output file. It has the same format as VCF but does not contain any headers. Default: 
STDOUT

=item B<-p>

Position (both ends of each sequence are checked).

=item B<-r>

(Optional) If specified, acts as the upper range for the position interval, i.e. '-p 71 -P 75' will
select all polimorphisms in th (71..75) interval inclusive. Default: the value for -p

=back

=head1 DESCRIPTION

The script selects all variants that start at the specified position from either end of the
sequence. The format of the output file is the same as VCF except it lacks headers.

=head1 AUTHOR

Roman Briskine, Universität Zürich

=cut

