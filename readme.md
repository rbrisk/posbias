# Positional bias in variant calls against draft reference assemblies

This file describes the pipeline used to generate the results described in the following paper.

Briskine RV, Shimizu KK. Positional bias in variant calls against draft reference assemblies. BMC Genomics. 2017;18:263. [DOI:10.1186/s12864-017-3637-2](http://dx.doi.org/10.1186/s12864-017-3637-2)


## Notes and abbreviations

In the literature describing de Bruijn graph approaches to assembly, `k-mer` length may refer to either the string length at the graph vertices or the overlap length on the graph edges. To avoid the confusion, we will use the first definition and denote such length as `k`. The length of the overlap on the graph edges will be denoted as `K`, i.e. `K = k - 1` for de Bruijn graphs.

_Bcon_ - _Boa constrictor_, snake species used in Assemblathon 2

_Mzeb_ - _Maylandia zebra_, fish species used in Assemblathon 2

_Sim_ - simulated data set comprised of chromosomes 1 and 2 from TAIR10


## 0. Environment and input data

### Computing environment

Most computations were run on a Debian server (v7 and later upgraded to v8) with 32 cores and 1 TiB of memory. The server uses SunGridEngine for job management and LMOD for software management. Each step of the pipeline indicates what additional applications and versions are used by providing module loading commands. All scripts reside in the local `src` directory. Logs are generally sent to the local `log` directory. Gaps in step numbering are caused by omission of the analyses that were not reported in the paper.

Occasionally, we also used a virtual machine with 16 cores and 128 GiB of memory running Ubuntu 14.04, LMOD and SLURM. The server setup was essentially the same as on the main computational server.

Unless otherwise stated or inferred, all commands are executed from the working directory using relative paths. Some applications fail to work properly with relative paths. In those cases, we use `$wd` variable to indicate the absolute path to the working directory. For instance, the absolute path to the input data is `$wd/00_input`.

All modules export a variable whose name matches the application name and points to the application prefix, e.g. `seq/picard` module exports `$picard` variable that points to picard installation directory. In addition some modules may export additional variables that point to `lib` and `include` directories within the installation prefix. Those variables have suffixes `_lib` and `_inc` respectively.

Some applications (e.g. KmerGenie) require `python2`. It will be provided via `pyenv`.

We use bash v4.3 and rely heavily on bashisms such as brace expansion and STDERR redirection. Therefore, some bash scripts and commands may fail in other shells or earlier versions of bash.

### External data

_Bcon_ and _Mzeb_ libraries are selected randomly among available data sets with 400 bp and 180 bp inserts respectively. The _Mzeb_ data set is necessary to evaluate ALLPATHS algorithm, which is quite popular but whose team did not submit a _Bcon_ entry.

- *ERR234373* is a _Bcon_ paired end library with nominal length of 400 bp sequenced on Illumina Genome Analyzer IIx (8.6G bases)
- *SRR077290* is a _Mzeb_ paired end library with nominal length of 283 bp sequenced on Illumina Genome Analyzer II (60.4G bases)

fastq-dump is a tool provided by NCBI's SRA for efficient data retrieval and conversion. Due to large data set sizes each command make take several hours to complete. The tool downloads an sra file into ~/ncbi/public/src and splits it into two fastq files with paired-end reads. The sra file may be deleted afterwards if it is no longe needed.

```shell
mkdir 00_input && cd 00_input
nice fastq-dump --split-files --gzip ERR234373 >& ../log/00_ERR234373.log &
nice fastq-dump --split-files --gzip SRR077290 >& ../log/00_SRR077290.log &
cd ..
```

Download Assemblathon 2 assemblies. The link for the alternative SOAPdenovo2 assembly (in supplementary file 3) seems to be dead; the data has been removed from the server (2016-01-14). We have downloaded the scaffolds while they were still available. Contigs should not have been affected because the mislabelled libraries were 4 kb and 10 kb ones. The script below also generates the list of samples for each data type.

```shell
src/00_downloadSequences
```


## 1. Prepare input data

GATK cannot handle compressed reference files. So, we need to uncompress them, build BWA index, fasta index, and picard dictionary. Another problem is that some contig data sets, e.g. Bcon_abyss_9C.fa, have duplicate names, so `picard` fails to build a dictionary. In case of ABySS, it seems to be a numbering problem. In case of SOAPdenovo, it's a design choice as they use scaffold names as contig names and put contig names as description. So, we have to rename contigs using sequential numbers as "C%06d". While at it, we can also remove blank lines from the fasta files.

```shell
module load java/jdk/7/79 seq/samtools/1.2 seq/picard/1.130
nice src/01_prepareReferences scaffold 00_input 00_input/scaffold/Bcon.txt 01_reference &> log/01_refScaffoldBcon &
nice src/01_prepareReferences scaffold 00_input 00_input/scaffold/Mzeb.txt 01_reference &> log/01_refScaffoldMzeb &
nice src/01_prepareReferences contig 00_input 00_input/contig/Bcon.txt 01_reference &> log/01_refContigBcon &
nice src/01_prepareReferences contig 00_input 00_input/contig/Mzeb.txt 01_reference &> log/01_refContigMzeb &
```


## 2. Variants

### Call variants

Note that BWA includes its own samtools binary. To make sure that we use the correct version, samtools should be loaded after BWA.

```shell
module load seq/bwa/0.7.12 seq/queue/3.4-0 seq/samtools/1.2 seq/picard/1.130
nice src/02_runDetect scaffold 01_reference 02_variants 00_input/scaffold/Bcon.txt 00_input/ERR234373_1.fastq.gz 00_input/ERR234373_2.fastq.gz 8 16 "-run " &> log/02_scaffoldBcon &
nice src/02_runDetect scaffold 01_reference 02_variants 00_input/scaffold/Mzeb.txt 00_input/SRR077290_1.fastq.gz 00_input/SRR077290_2.fastq.gz 8 24 "-run " &> log/02_scaffoldMzeb &
nice src/02_runDetect contig 01_reference 02_variants 00_input/contig/Bcon.txt 00_input/ERR234373_1.fastq.gz 00_input/ERR234373_2.fastq.gz 8 24 "-run " &> log/02_contigBcon &
nice src/02_runDetect contig 01_reference 02_variants 00_input/contig/Mzeb.txt 00_input/SRR077290_1.fastq.gz 00_input/SRR077290_2.fastq.gz 8 24 "-run " &> log/02_contigMzeb &
```

### Generate graphs

As a side effect, 02_runMakeGraphs also generates `SeqLength.txt` file for each assembly.

```shell
nice src/02_runMakeGraphs -d 02_variants -t scaffold -r 00_input/scaffold/Bcon.txt &> log/02_graphs_Bcon_scaffold &
nice src/02_runMakeGraphs -d 02_variants -t scaffold -r 00_input/scaffold/Mzeb.txt &> log/02_graphs_Mzeb_scaffold &
nice src/02_runMakeGraphs -d 02_variants -t contig -r 00_input/contig/Bcon.txt &> log/02_graphs_Bcon_contig &
nice src/02_runMakeGraphs -d 02_variants -t contig -r 00_input/contig/Mzeb.txt &> log/02_graphs_Mzeb_contig &
```

In some cases, the script could not correctly guess `k` because the count was high but not the highest. We will fix those cases manually. As of 2017-02-01, you can also specify the expected k values for `02_tidyData.R` script.

```shell
nice src/02_reMakeGraphs &> log/02_reMakeGraphs &
```

### Combine data

Combined data set will facilitate the generation of faceted graphs.

```shell
src/02_tidyData.R -p 02_variants/contig -r 00_input/contig/Bcon.txt --fnVariants=filtered.txt --fnSeqLen=SeqLength.txt -o 02_variants/contig/Bcon.txt | tee log/02_tidy_Bcon_contig
src/02_tidyData.R -p 02_variants/contig -r 00_input/contig/Mzeb.txt --fnVariants=filtered.txt --fnSeqLen=SeqLength.txt -o 02_variants/contig/Mzeb.txt | tee log/02_tidy_Mzeb_contig

src/02_tidyData.R -p 02_variants/scaffold -r 00_input/scaffold/Bcon.txt --fnVariants=filtered.txt --fnSeqLen=SeqLength.txt -o 02_variants/scaffold/Bcon.txt | tee log/02_tidy_Bcon_scaffold
src/02_tidyData.R -p 02_variants/scaffold -r 00_input/scaffold/Mzeb.txt --fnVariants=filtered.txt --fnSeqLen=SeqLength.txt -o 02_variants/scaffold/Mzeb.txt | tee log/02_tidy_Mzeb_scaffold
```

Compress to save space

```shell
for i in 02_variants/contig/*.txt; do echo $i; xz -T 8 $i; done
for i in 02_variants/scaffold/*.txt; do echo $i; xz -T 8 $i; done
```

### Faceted graphs

```shell
for species in Bcon Mzeb; do
	for varT in SNP INDEL; do
		for seqT in contig scaffold; do
			echo $species $varT $seqT
			src/02_makeGraphsFacet.R -i 02_variants/$seqT/${species}.txt.xz -o graphs/02_variants/$seqT/$species --selectType $varT --imgTheme "theme_bw" --histFill "#333333" --alpha "0.5" --imgType pdf --imgPrefix $varT
		done
	done
done
```

Graphs grouped by species take too much space. It might be better to combine both species into a single faceted graph.

```shell
xzcat 02_variants/contig/{Bcon,Mzeb}.txt.xz | xz -c > 02_variants/contig/All.txt.xz
xzcat 02_variants/scaffold/{Bcon,Mzeb}.txt.xz | grep -v '^CHROM' | xz -c > 02_variants/scaffold/All.txt.xz
for varT in SNP INDEL; do
	for seqT in contig scaffold; do
		echo $varT $seqT
		src/02_makeGraphsFacet.R -i 02_variants/$seqT/All.txt.xz -o graphs/02_variants/$seqT/All --selectType $varT --imgTheme "theme_bw" --histFill "#333333" --alpha "0.5" --imgType pdf --imgPrefix $varT --imgWidth=16.9 --imgHeight=20.3
	done
done
rm 02_variants/{contig,scaffold}/All.txt.xz
```


### Strict coverage threshold

To make sure that high coverage does not affect the results, we applied a much stricter coverage threshold calculated using Lander-Waterman equation. For Bcon, the expected coverage would be `35,594,924 reads * 2 * 121 bp / 1.6 Gb = 5.38x`. For Mzeb, it is `37,508,730 reads * 2 * 101 bp / 1 Gb = 7.58x`.

```shell
xzcat 02_variants/contig/Bcon.txt.xz | perl -nale 'if ($. == 1 || $F[10] + $F[15] <= 5) { print $_ }' > 02_variants/contig/AllStrictCov.txt
xzcat 02_variants/contig/Mzeb.txt.xz | perl -nale 'if ($. > 1 && $F[10] + $F[15] <= 8) { print $_ }' >> 02_variants/contig/AllStrictCov.txt
src/02_makeGraphsFacet.R -i 02_variants/contig/AllStrictCov.txt -o graphs/02_variants/contig/AllStrictCov --selectType $varT --imgTheme "theme_bw" --histFill "#333333" --alpha "0.5" --imgType pdf --imgPrefix SNP --imgWidth=16.9 --imgHeight=20.3
```

The bias is still present even after such conservative filtering.




## 3. Simulate reads

To be able to trace the origin of the aligned reads, we will simulate short reads using chromosomes 1 and 2 from TAIR 10. In addition, we will use this data set to show that the peak moves with the change of `k`. The chromosomes were chosen because they appear to contain many duplicate genes compared to other chromosome pairs.

We will create a single short insert library (180 bp) and a long insert library (3 kbp). Such sizes will allow us to run ALLPATHS-LG and to perform the scaffolding afterwards. 

The combined size of Chr 1 and Chr 2 is ~50 Mbp. Using 15 mln 2 x 100 bp reads yielded 60x coverage by the short insert library. The long insert library will have 5 mln reads resulting in 20x coverage. 

The `genome.info` will be supplied to samtools, so that it could generate the header. If the tab at the end is omitted, samtools generates incorrect header because it does not chomp the line.

### Download chromosomes

```shell
mkdir 03_simAtha
curl -L ftp://ftp.arabidopsis.org/home/tair/Sequences/whole_chromosomes/TAIR10_chr1.fas >  03_simAtha/genome.fa
curl -L ftp://ftp.arabidopsis.org/home/tair/Sequences/whole_chromosomes/TAIR10_chr2.fas >> 03_simAtha/genome.fa
printf 'Chr1\t30427671\t\n' >  03_simAtha/genome.info
printf 'Chr2\t19698289\t\n' >> 03_simAtha/genome.info
```

The lengths of chromosomes 1 and 2 are 30,427,671 and 19,698,289 respectively.

### Index the data

Index the file and create BLAST DB.

```shell
module load seq/samtools/1.2 bio/blast/2.2.30
samtools faidx 03_simAtha/00_input/genome.fa
makeblastdb -in 03_simAtha/00_input/genome.fa -dbtype nucl
```

### Simulate reads

Simulate reads with SimSeq. To find the true location of reads, we need a bam file sorted by read name. We will use the default standard deviation of `20` for the short insert library. Since we do not care about duplicates, we will keep their probability at the default level by using the value of `0.0`.

```shell
module load java/jdk/8/40 seq/simseq/20120615 seq/samtools/1.2 seq/picard/1.130
src/03_runSimSeq
```

### Check quality

```shell
module load seq/fastqc/0.11.4
sbatch -o log/03_fastqc -t 0:30:0 src/03_jobFastQC
```

### Estimate k-mer size

```shell
module purge
pyenv shell 2.7.8
module load seq/kmergenie
mkdir 03_simAtha/kmergenie
kmergenie <( ls -1 03_simAtha/Atha_200_R[12].fastq.gz) -l 39 -k 99 -s 10 -t 8 -o 03_simAtha/kmergenie/Atha
# It keeps failing to run the refining step
kmergenie <( ls -1 03_simAtha/Atha_200_R[12].fastq.gz) -l 63 -k 75 -s 2 -t 8 -o 03_simAtha/kmergenie/Atha_refine
```

The maximum is achieved at `K=69`. The refinement is not concave but the best value remains `K=69`. The next best values are tied at `K=67` and `K=71`. We will use the latter to show that the peak shifts with the change of `K`.


## 4. Assemble simulated data

```shell
mkdir -p 04_simAssembly/soap_K{69,71}
mkdir -p 04_simAssembly/allpaths
mkdir -p 04_simAssembly/sga
```

### SOAPdenovo2 K=69

This is the best `K` value.

```shell
module load seq/soapdenovo/2.04
nice SOAPdenovo-127mer all -K 69 -d 1 -D 1 -R -p 8 -s 04_simAssembly/soap2.config -o 04_simAssembly/soap_K69/SimAtha_K69 &> log/04_soap_K69 &
```

### SOAPdenovo2 K=71

This is the second best `K` value tied with `K=67`.

```shell
module load seq/soapdenovo/2.04
nice SOAPdenovo-127mer all -K 71 -d 1 -D 1 -R -p 8 -s 04_simAssembly/soap2.config -o 04_simAssembly/soap_K71/SimAtha_K71 &> log/04_soap_K71 &
```

### ALLPATHS-LG

First, create the `libs.csv` file that describes the libraries and `groups.csv` file that lists the input files. Then generate the cache. `CACHE_DIR` must have an absolute path. `HOSTS` can be used for parallelisation, e.g. number of cores to use.

Prepare cache

```shell
module load util/localeC seq/picard/1.130 seq/allpathslg/52293
mkdir -p 04_simAssembly/allpaths/cache
CacheLibs.pl CACHE_DIR=$wd/PosBias/04_simAssembly/allpaths/cache IN_LIBS_CSV=04_simAssembly/allpaths/libs.csv ACTION=Add
CacheGroups.pl CACHE_DIR=$wd/PosBias/04_simAssembly/allpaths/cache PICARD_TOOLS_DIR=$picard IN_GROUPS_CSV=04_simAssembly/allpaths/groups.csv TMP_DIR=$wd/tmp ACTION=Add HOSTS=8
```

Assemble. `MIN_CONTIG` behaviour is strange because 200 bp threshold results in reporting sequences shorter than 200. By default, only contigs longer than 1000 are reported.

```shell
module load util/localeC seq/picard/1.130 seq/allpathslg/52293
mkdir -p 04_simAssembly/allpaths/data
CacheToAllPathsInputs.pl CACHE_DIR=$wd/PosBias/04_simAssembly/allpaths/cache GROUPS="{PE200,MP030}" DATA_DIR=$wd/PosBias/04_simAssembly/allpaths/data PLOIDY=2
nice RunAllPathsLG PRE=$wd/PosBias/04_simAssembly DATA_SUBDIR=data RUN=default SUBDIR=default REFERENCE_NAME=allpaths THREADS=8 MIN_CONTIG=200 &> log/04_allpaths2 &
```


### SGA

Since SGA fish assembly has a peak at 121, it is hard to tell whether it depends on the overlap length selection (`tau` in the paper, `m` in the software) or some other parameter. It would be useful to create two SGA assemblies and call variants. To prevent having output files all over the place, you have to change into the output directory.

Except for the last step, the overlap value (`tau`) is not used anywhere. Therefore, we can put everything in one directory and simply add the suffix at the last step. For our purposes, the value of `k` (k-mer length for correction) does not really matter.

- `-k 41` is the k-mer length for error correction
- `-m 55` in the overlap command is the minimum overlap that cannot be greater than `-m` value specified in assemble command
- `-m 73`, `-m 75` and `-m 77` in the assemble commands is the minimum string overlap for assembly.

```shell
module load seq/sga/0.10.14
mkdir 04_simAssembly/sga
cd 04_simAssembly/sga
logd=../../log/04_sga
nice sga preprocess --pe-mode 1 -o 01_Atha_200.fastq ../../03_simAtha/Atha_200_R{1,2}.fastq.gz &> $logd/01_preprocess &
# Error correction
nice sga index -a ropebwt -t 10 --no-reverse 01_Atha_200.fastq &
nice sga correct -k 41 --discard --learn -t 10 -o 02_reads.ec.k41.fastq 01_Atha_200.fastq &> $logd/02_ec
sga index -a ropebwt -t 10 02_reads.ec.k41.fastq
# Contig assembly
nice sga filter -x 2 -t 10 --homopolymer-check --low-complexity-check -o 03_reads.filtered.k41.fa 02_reads.ec.k41.fastq &> $logd/03_filter &
nice sga fm-merge -m 55 -t 10 -o 04_merged.k41.fa 03_reads.filtered.k41.fa &> $logd/04_merge &
sga index -d 5000000 -t 10 04_merged.k41.fa
nice sga rmdup -t 10 -o 05_rmdup.k41.fa 04_merged.k41.fa &> $logd/05_rmdup &
nice sga overlap -m 55 -t 10 05_rmdup.k41.fa &> $logd/05_overlap &
nice sga assemble -m 73 -r 10 -o 06_assembly_m73 05_rmdup.k41.asqg.gz &> $logd/06_assemble_73 &
nice sga assemble -m 75 -r 10 -o 06_assembly_m75 05_rmdup.k41.asqg.gz &> $logd/06_assemble_75 &
nice sga assemble -m 77 -r 10 -o 06_assembly_m77 05_rmdup.k41.asqg.gz &> $logd/06_assemble_77 &
```

The longest contig has the same length in all cases but N50 increases with the increase in the value of the `-m` parameter specified for the `assemble` command. Therefore, we will use the `-m 75` and `-m 77` assemblies.

Scaffolding

```shell
module load seq/bwa/0.7.12 seq/abyss/1.3.7 seq/sga/0.10.14 seq/samtools/1.2
logd=../../log/04_sga
pyenv shell 2.7.8
pip install ruffus pysam
sga-align -t 8 --name 07_aligned_3000_m75 06_assembly_m75-contigs.fa ../../03_simAtha/Atha_3000_R{1,2}.fastq.gz
sga-align -t 8 --name 07_aligned_3000_m77 06_assembly_m77-contigs.fa ../../03_simAtha/Atha_3000_R{1,2}.fastq.gz
sga-bam2de.pl -n 3 --prefix 08_3000_m75 07_aligned_3000_m75.bam
sga-bam2de.pl -n 3 --prefix 08_3000_m77 07_aligned_3000_m77.bam
sga-astat.py -m 200 07_aligned_3000_m75.refsort.bam > 09_3000_m75.astat
sga-astat.py -m 200 07_aligned_3000_m77.refsort.bam > 09_3000_m77.astat
sga scaffold -m 200 -a 09_3000_m75.astat -g 06_assembly_m75-graph.asqg.gz --mate-pair=08_3000_m75.de -o 10_m75.scaf 06_assembly_m75-contigs.fa
sga scaffold -m 200 -a 09_3000_m77.astat -g 06_assembly_m77-graph.asqg.gz --mate-pair=08_3000_m77.de -o 10_m77.scaf 06_assembly_m77-contigs.fa
sga scaffold2fasta --write-unplaced -m 200 -o 11_scaffolds_m75.fa --use-overlap -a 06_assembly_m75-graph.asqg.gz 10_m75.scaf
sga scaffold2fasta --write-unplaced -m 200 -o 11_scaffolds_m77.fa --use-overlap -a 06_assembly_m77-graph.asqg.gz 10_m77.scaf
```

### Move to one directory

The references should be in one directory, so that we could re-use existing scripts. The `Sim_list.txt` file contains the list of all assembly names.

```shell
mkdir -p 04_simAssembly/contig
cp -a 04_simAssembly/soap_K69/SimAtha_K69.contig 04_simAssembly/contig/Sim_soap_K69.fa
cp -a 04_simAssembly/soap_K71/SimAtha_K71.contig 04_simAssembly/contig/Sim_soap_K71.fa
cp -a 04_simAssembly/allpaths/data/default/ASSEMBLIES/default/final.contigs.fasta 04_simAssembly/contig/Sim_allp.fa
cp -a 04_simAssembly/sga/06_assembly_m75-contigs.fa 04_simAssembly/contig/Sim_sga_m75.fa
cp -a 04_simAssembly/sga/06_assembly_m77-contigs.fa 04_simAssembly/contig/Sim_sga_m77.fa
for i in 04_simAssembly/contig/*.fa; do echo $i; gzip $i; done
ls 04_simAssembly/contig/*.gz | sed -r 's/^.+(Sim_[^.]+).fa.gz$/\1/' > 04_simAssembly/Sim_list.txt
```

Scaffolds. I added `Atha.fa` containing Chr 1 and Chr 2 because for additional variant evaluation in step 10 but it was not very useful in the end. The list for position transformation (coordinate mapping) will contain only Sim_allp and Sim_soap_K69. The transformation for SGA is very complex and the transformation for Sim_soap_K71 would probably be redundant.

```shell
mkdir -p 04_simAssembly/scaffold
cp -a 04_simAssembly/soap_K69/SimAtha_K69.scafSeq 04_simAssembly/scaffold/Sim_soap_K69.fa
cp -a 04_simAssembly/soap_K71/SimAtha_K71.scafSeq 04_simAssembly/scaffold/Sim_soap_K71.fa
cp -a 04_simAssembly/allpaths/data/default/ASSEMBLIES/default/final.assembly.fasta 04_simAssembly/scaffold/Sim_allp.fa
cp -a 04_simAssembly/sga/11_scaffolds_m75.fa 04_simAssembly/scaffold/Sim_sga_m75.fa
cp -a 04_simAssembly/sga/11_scaffolds_m77.fa 04_simAssembly/scaffold/Sim_sga_m77.fa
cp -a 03_simAtha/genome.fa 04_simAssembly/scaffold/Atha.fa
for i in 04_simAssembly/scaffold/*.fa; do echo $i; gzip $i; done
cp -a 04_simAssembly/Sim_list{,_Atha}.txt
echo Atha >> 04_simAssembly/Sim_list_Atha.txt
echo -e 'Sim_allp\nSim_soap_K69' > 04_simAssembly/Sim_list_transf.txt
```



## 5. Prepare simulated input data

This is the same as step 1 but we will also filter out sequences shorter than 200 bp. Such sequences are not informative.

```shell
module load java/jdk/7/79 seq/samtools/1.2 seq/picard/1.130 seq/bwa/0.7.12
nice src/01_prepareReferences contig 04_simAssembly 04_simAssembly/Sim_list.txt 05_simReference 200 &> log/05_simRefContig &
nice src/01_prepareReferences scaffold 04_simAssembly 04_simAssembly/Sim_list.txt 05_simReference 200 &> log/05_simRefScaffold &
```

Build maps of original contig ids to the new contig ids. The original contig ids are used in the scaffold definition files (`.contigPosInscaff` and `final.summary`) but the contig fasta files have the new contig ids. Therefore, we need contig id maps to build the scaffold-to-contig coordinate transformation maps.

```shell
src/05_makeContigNameMap.pl -i 05_simReference/contig/Sim_soap_K69.fa -o 05_simReference/contig/Sim_soap_K69.nameMap
src/05_makeContigNameMap.pl -i 05_simReference/contig/Sim_soap_K71.fa -o 05_simReference/contig/Sim_soap_K71.nameMap
src/05_makeContigNameMap.pl -i 05_simReference/contig/Sim_allp.fa -p '^contig_(.+)$' -o 05_simReference/contig/Sim_allp.nameMap
```

Build transformation maps of scaffold coordinates to contig coordinates.

```shell
src/05_makeScafToContigMap.pl -i 04_simAssembly/soap_K69/SimAtha_K69.contigPosInscaff -c 04_simAssembly/soap_K69/SimAtha_K69.contig -t soap -m 05_simReference/contig/Sim_soap_K69.nameMap -o 05_simReference/scaffold/Sim_soap_K69.coordMap
src/05_makeScafToContigMap.pl -i 04_simAssembly/soap_K71/SimAtha_K71.contigPosInscaff -c 04_simAssembly/soap_K71/SimAtha_K71.contig -t soap -m 05_simReference/contig/Sim_soap_K71.nameMap -o 05_simReference/scaffold/Sim_soap_K71.coordMap
src/05_makeScafToContigMap.pl -i 04_simAssembly/allpaths/data/default/ASSEMBLIES/default/final.summary -c 04_simAssembly/allpaths/data/default/ASSEMBLIES/default/final.contigs.fasta -t allpaths -m 05_simReference/contig/Sim_allp.nameMap -o 05_simReference/scaffold/Sim_allp.coordMap
```



## 6. Call variants for simulated data

### Call variants

Note that BWA includes its own samtools binary. To make sure that we use the correct version, samtools should be loaded after BWA.

```shell
module load seq/bwa/0.7.12 seq/queue/3.4-0 seq/samtools/1.2 seq/picard/1.130
nice src/02_runDetect contig 05_simReference 06_simVariants 04_simAssembly/Sim_list.txt 03_simAtha/Atha_200_R{1,2}.fastq.gz 8 16 "-run " &> log/06_sim_contig &

nice src/02_runDetectSlurm scaffold 05_simReference 06_simVariants 04_simAssembly/Sim_list_Atha.txt 03_simAtha/Atha_200_R{1,2}.fastq.gz 16 16 "-run " &> log/06_sim_scaffold &
```

### Transform scaffold coordinates to contig coordinates

The output will be in a separate `transf` directory. This command ignores contigs shorter than 200 bp, i.e. the output would contain only positions that map to contigs longer than 200 bp. A position located at the intersection of two contigs will yield two rows in the output, one for each contig.

```shell
src/06_runTransform -d 06_simVariants -r 04_simAssembly/Sim_list_transf.txt -t 200
```

The following command will map all positions regardless of the target contig length. The output will be placed into `transf_all` directory. But first we need to retrieve the lengths for all available contigs.

```shell
sed -nr '/^>/ s/^>([0-9]+) length ([0-9]+).*/\1\t\2/p' 04_simAssembly/soap_K69/SimAtha_K69.contig >> 06_simVariants/contig/Sim_soap_K69/SeqLength.txt
src/06_runTransform -d 06_simVariants -r 04_simAssembly/Sim_list_transf.txt -m transf_all
```

Combine the position maps so that we could add scaffold information to the transf and transf_all variants. The second sed removes repeated headers if they are present.

```shell
for i in transf transf_all; do
	if [[ -f 06_simVariants/$i/PositionMap.txt ]]; then rm 06_simVariants/$i/PositionMap.txt; fi
	for j in `cat 04_simAssembly/Sim_list_transf.txt`; do
		sed -re '1 s/^/RefName\t/' -e '2,$ s/^/'$j'\t/' 06_simVariants/$i/$j/PositionMap.txt >> 06_simVariants/$i/PositionMap.txt
	done
	sed -n -i -e '1 p' -e '/RefName/!p' 06_simVariants/$i/PositionMap.txt
done
```

Find intersects among contig, transf, transf_all, and scaffold. In particularly, we want to know how many SNPs could not be mapped from scaffold to contig and how many unique SNPs are present in the scaffold data set. The mapping may fail due to length thresholding (we only used contigs >200 bp) and because Sim_allp extends some scaffolds beyond the length specified in `final.summary`. To get the number of SNPs that were in non-_k_ position in scaffolds but appear in position `k` after transformation, you need to subtract Shared SNPs between contig and scaffold from Shared SNPS between contig and transf. These values are in the Masked column (`k` position SNPs appear as non-_k_ position SNPs, hence the term 'masking'). For example, for Sim_allp contigs that will be `18 - 6 = 12`.

```shell
src/06_findIntersect.R -d 06_simVariants -o 06_simVariants/intersect.txt -t 'SNP' --thresGQ=40
```


### Generate graphs

```shell
nice src/02_runMakeGraphs -d 06_simVariants -t contig -r 04_simAssembly/Sim_list.txt &> log/06_graphs_sim_contig &
nice src/02_runMakeGraphs -d 06_simVariants -t scaffold -r 04_simAssembly/Sim_list.txt &> log/06_graphs_sim_scaffold &
```

Make graphs for variants with transformed coordinates. Note that it makes the wrong `k` guess for Sim_allp INDELs.

```shell
src/02_runMakeGraphs -d 06_simVariants -t transf -r 04_simAssembly/Sim_list_transf.txt -v filtered.txt | tee log/06_graph_transf
src/02_runMakeGraphs -d 06_simVariants -t transf_all -r 04_simAssembly/Sim_list_transf.txt -v filtered.txt | tee log/06_graph_transf_all
```

### Combine data

Combine the data to facilitate the generation of faceted graphs.

```shell
for seqT in contig scaffold transf transf_all; do
	if [[ $seqT == @(transf|transf_all) ]]; then sfx="_transf"; else sfx=""; fi
	src/02_tidyData.R -p 06_simVariants/$seqT -r 04_simAssembly/Sim_list${sfx}.txt --fnVariants=filtered.txt --fnSeqLen=SeqLength.txt -o 06_simVariants/$seqT/variants.txt | tee log/06_tidy_$seqT
done
```


### Intersection graphs

Graph the intersection between contig and transformed scaffold variants.

```shell
src/06_runGraphVarIntersect -a 06_simVariants/contig -l "Contig Only" -b 06_simVariants/transf -m "Scaffold (Transformed) Only" -r 04_simAssembly/Sim_list_transf.txt -o graphs/06_simVariants/contig_transf | tee log/06_graphIntersectCS
```

Venn diagrams

```shell
for r in `cat 04_simAssembly/Sim_list_transf.txt`; do
	src/06_graphVarVenn.R -a 06_simVariants/contig/variants.txt --labelA "Contig" -b 06_simVariants/transf/variants.txt --labelB "Scaffold" -o graphs/06_simVariants/contig_transf/${r}/bw_venn_SNP.pdf -t SNP --refName $r
done
```


### Faceted graphs.

We want to have Sim_sga_m77 under Sim_sga_m75 and Sim_soap_K71 under Sim_soap_K69 while Sim_allp would occupy the remaining available slot. However, `transf` directories do not have SGA entries, so we have to specify different panel order for those cases.

```shell
for seqT in contig scaffold; do
	for varT in SNP INDEL; do
		echo $seqT $varT
		src/02_makeGraphsFacet.R -i 06_simVariants/$seqT/variants.txt -o graphs/06_simVariants/$seqT/All --posRangeStr 'c(20,120)' --selectType $varT --imgTheme "theme_bw" --histFill "#333333" --alpha "0.5" --imgType pdf --imgPrefix $varT --imgWidth 16.9 --imgHeight 16.9 --panelOrder "2,4,3,5,1"
	done
done
```

Graphs for the mapped data set only have two panels, so they need reduced height.

```shell
for seqT in transf; do
	for varT in SNP INDEL; do
		echo $seqT $varT
		src/02_makeGraphsFacet.R -i 06_simVariants/$seqT/variants.txt -o graphs/06_simVariants/$seqT/All --posRangeStr 'c(20,120)' --selectType $varT --imgTheme "theme_bw" --histFill "#333333" --alpha "0.5" --imgType pdf --imgPrefix $varT --imgWidth 16.9 --imgHeight 8.45
	done
done
```



## 7. Investigate simulated read alignments

### Extract subsets

Extract list of variants at position `k` from either end.

```shell
mkdir -p 07_simLoci/contig/{Sim_soap_K69,Sim_soap_K71,Sim_sga_m75,Sim_sga_m77,Sim_allp}
src/07_selectPosVariants.pl -i <(gunzip -c 06_simVariants/contig/Sim_soap_K69/filtered.vcf.gz) -o 07_simLoci/contig/Sim_soap_K69/variants.txt -p 70
src/07_selectPosVariants.pl -i <(gunzip -c 06_simVariants/contig/Sim_soap_K71/filtered.vcf.gz) -o 07_simLoci/contig/Sim_soap_K71/variants.txt -p 72
src/07_selectPosVariants.pl -i <(gunzip -c 06_simVariants/contig/Sim_sga_m75/filtered.vcf.gz) -o 07_simLoci/contig/Sim_sga_m75/variants.txt -p 100
src/07_selectPosVariants.pl -i <(gunzip -c 06_simVariants/contig/Sim_sga_m77/filtered.vcf.gz) -o 07_simLoci/contig/Sim_sga_m77/variants.txt -p 100
src/07_selectPosVariants.pl -i <(gunzip -c 06_simVariants/contig/Sim_allp/filtered.vcf.gz) -o 07_simLoci/contig/Sim_allp/variants.txt -p 96
```


### Retrieve read info

For each selected variant position, find all reads that align and their original positions in Atha.

```shell
module load java/jdk/8/40 seq/htsjdk/1.140 dev/scala/2.11.6
# Sim_soap
JAVA_OPTS=-Xmx32G scala -classpath "$htsjdk/*" src/ExtractReadInfo.scala 07_simLoci/contig/Sim_soap_K69/variants.txt 06_simVariants/contig/Sim_soap_K69/duplicatesMarked.bam 03_simAtha/Atha_200.bam 07_simLoci/contig/Sim_soap_K69/readInfo.txt
JAVA_OPTS=-Xmx32G scala -classpath "$htsjdk/*" src/ExtractReadInfo.scala 07_simLoci/contig/Sim_soap_K71/variants.txt 06_simVariants/contig/Sim_soap_K71/duplicatesMarked.bam 03_simAtha/Atha_200.bam 07_simLoci/contig/Sim_soap_K71/readInfo.txt

# Sim_sga
JAVA_OPTS=-Xmx32G scala -classpath "$htsjdk/*" src/ExtractReadInfo.scala 07_simLoci/contig/Sim_sga_m75/variants.txt 06_simVariants/contig/Sim_sga_m75/duplicatesMarked.bam 03_simAtha/Atha_200.bam 07_simLoci/contig/Sim_sga_m75/readInfo.txt
JAVA_OPTS=-Xmx32G scala -classpath "$htsjdk/*" src/ExtractReadInfo.scala 07_simLoci/contig/Sim_sga_m77/variants.txt 06_simVariants/contig/Sim_sga_m77/duplicatesMarked.bam 03_simAtha/Atha_200.bam 07_simLoci/contig/Sim_sga_m77/readInfo.txt

# Sim_allp
JAVA_OPTS=-Xmx32G scala -classpath "$htsjdk/*" src/ExtractReadInfo.scala 07_simLoci/contig/Sim_allp/variants.txt 06_simVariants/contig/Sim_allp/duplicatesMarked.bam 03_simAtha/Atha_200.bam 07_simLoci/contig/Sim_allp/readInfo.txt
```


### Origin Counts

Count the number of origins for well-aligned reads that support the variant allele. We only consider the primary alignments, which ensures that the assembly does not contain duplicate sequences where the reads would align better. The CIGAR filter (CIGAR=100M) ensures that we exclude any strange alignments that have insertions or deletions. Finally, MQ filter allows us to keep the number of mismatches low.

The count of 0 indicates that the variant causing reads either are not well aligned (different origin from the reference reads) or contain sequencing errors (same origin as the reference reads). The number of variants that have the origin count of 0 is shown in the OrigZero column below. Positive count shows the number of regions where the variant causing reads are coming from after excluding the origins of the reference reads. The number of variants that have positive origin count is shown in OrigPos column below.

```shell
unset append
for i in Sim_allp Sim_soap_K69 Sim_soap_K71 Sim_sga_m75 Sim_sga_m77; do
	echo $i
	src/07_summariseOriginCnt.R -i 07_simLoci/contig/$i/readInfo.txt -o 07_simLoci/contig/variantReadOrigins.txt --vartype SNP --minMQ 40 --cigar 100M --label $i $append
	append='--append'
done
unset append
```

| Assembly     | OrigPos | OrigZero | LowQFreq |
|:-------------|--------:|---------:|---------:|
| Sim_allp     |      57 |        6 |   0.0588 |
| Sim_soap_K69 |    1391 |       90 |   0.0324 |
| Sim_soap_K71 |    1390 |       79 |   0.0284 |
| Sim_sga_m75  |     477 |       26 |   0.0273 |
| Sim_sga_m77  |     482 |       30 |   0.0311 |

As expected, most variants at the `k` position have more than one origin even when we filter out poorly aligned reads. Less than 6% of variants are caused by poorly aligned reads or potentially by simulated sequencing errors. Moreover, many variants have only 1-3 additional origins (see the graph) indicating that copy number does not have to be very high to produce variants.

```shell
src/07_graphOriginCntHist.R -i 07_simLoci/contig/variantReadOrigins.txt -o graphs/07_simLoci/contig/variantReadOrigins.pdf --imgTheme=theme_bw --colour="#333333" --panelOrder="2,3,4,5,1"
```


## 8. Filter by length

Presence of very short sequences may potentially have a confounding effect on variant calling. We will remove all sequences shorter than 500 bp from both scaffold and contig data sets. The threshold was selected to make sure that the genome coverage is not significantly reduced. Less than ~3% and ~6% of bases will be removed from scaffold and contig assemblies respectively. Mzeb_allp_6C and Bcon_sga_7C scaffold data sets are not affected because their minimum sequence length is 959 and 500 respectively. Therefore, we will not re-analyse them. LF stands for Length Filter.

Assemblies have the following minimum sequence lengths.

| Assembler | Species | Scaffold | Contig |
|:----------|:--------|---------:|-------:|
| abyss_9C  | Bcon    |      150 |     56 |
| abyss_7C  | Mzeb    |      185 |     80 |
| allp_6C   | Mzeb    |      959 |     40 |
| merac_6C  | Bcon    |       82 |     82 |
| phus_5C   | Bcon    |      300 |      3 |
| sga_7C    | Bcon    |      500 |     84 |
| soap_11C  | Bcon    |      100 |      8 |
| soap_11E  | Mzeb    |      100 |      8 |

```shell
module load java/jdk/7/79 seq/samtools/1.2 seq/picard/1.130
mkdir -p 08_referenceLF/{scaffold,contig}
cat 00_input/scaffold/Bcon.txt 00_input/scaffold/Mzeb.txt > 08_referenceLF/scaffold/All.txt
cat 00_input/contig/Bcon.txt 00_input/contig/Mzeb.txt > 08_referenceLF/contig/All.txt
nice src/01_prepareReferences scaffold 00_input 08_referenceLF/scaffold/All.txt 08_referenceLF 500 &> log/08_ref_scaffold &
nice src/01_prepareReferences contig 00_input 08_referenceLF/contig/All.txt 08_referenceLF 500 &> log/08_ref_contig &
```

Remove the files of the unaffected references and remove them from the lists. Separate by species as the reads are species specific.

```shell
rm 08_referenceLF/scaffold/{Mzeb_allp_6C,Bcon_sga_7C}.*
grep Bcon 08_referenceLF/scaffold/All.txt | grep -v Bcon_sga_7C > 08_referenceLF/scaffold/Bcon.txt
grep Mzeb 08_referenceLF/scaffold/All.txt | grep -v Mzeb_allp_6C > 08_referenceLF/scaffold/Mzeb.txt
grep Bcon 08_referenceLF/contig/All.txt > 08_referenceLF/contig/Bcon.txt
grep Mzeb 08_referenceLF/contig/All.txt > 08_referenceLF/contig/Mzeb.txt
```



## 9. Variants on length-filtered assemblies


### Call variants

```shell
module load seq/bwa/0.7.12 seq/queue/3.4-0 seq/samtools/1.2 seq/picard/1.130
nice src/02_runDetect scaffold 08_referenceLF 09_variantsLF 08_referenceLF/scaffold/Bcon.txt 00_input/ERR234373_1.fastq.gz 00_input/ERR234373_2.fastq.gz 8 16 "-run " &> log/09_scaffoldBcon &
nice src/02_runDetect scaffold 08_referenceLF 09_variantsLF 08_referenceLF/scaffold/Mzeb.txt 00_input/SRR077290_1.fastq.gz 00_input/SRR077290_2.fastq.gz 8 24 "-run " &> log/09_scaffoldMzeb &
nice src/02_runDetect contig 08_referenceLF 09_variantsLF 08_referenceLF/contig/Bcon.txt 00_input/ERR234373_1.fastq.gz 00_input/ERR234373_2.fastq.gz 8 24 "-run " &> log/09_contigBcon &
nice src/02_runDetect contig 08_referenceLF 09_variantsLF 08_referenceLF/contig/Mzeb.txt 00_input/SRR077290_1.fastq.gz 00_input/SRR077290_2.fastq.gz 8 24 "-run " &> log/09_contigMzeb &
```

### Combine the data to facilitate faceted graph building.

```shell
src/02_tidyData.R -p 09_variantsLF/contig -r 08_referenceLF/contig/Bcon.txt --fnVariants=filtered.txt --fnSeqLen=SeqLength.txt -o 09_variantsLF/contig/Bcon.txt | tee log/09_tidy_Bcon_contig
src/02_tidyData.R -p 09_variantsLF/contig -r 08_referenceLF/contig/Mzeb.txt --fnVariants=filtered.txt --fnSeqLen=SeqLength.txt -o 09_variantsLF/contig/Mzeb.txt | tee log/09_tidy_Mzeb_contig

src/02_tidyData.R -p 09_variantsLF/scaffold -r 08_referenceLF/scaffold/Bcon.txt --fnVariants=filtered.txt --fnSeqLen=SeqLength.txt -o 09_variantsLF/scaffold/Bcon.txt | tee log/09_tidy_Bcon_scaffold
src/02_tidyData.R -p 09_variantsLF/scaffold -r 08_referenceLF/scaffold/Mzeb.txt --fnVariants=filtered.txt --fnSeqLen=SeqLength.txt -o 09_variantsLF/scaffold/Mzeb.txt | tee log/09_tidy_Mzeb_scaffold
```

### Compress the output

```shell
for i in 09_variantsLF/contig/*.txt; do echo $i; xz -T 8 $i; done
for i in 09_variantsLF/scaffold/*.txt; do echo $i; xz -T 8 $i; done
```

### Generate graphs

```shell
nice src/02_runMakeGraphs -d 09_variantsLF -t scaffold -r 08_referenceLF/scaffold/Bcon.txt &> log/09_graphs_Bcon_scaffold &
nice src/02_runMakeGraphs -d 09_variantsLF -t scaffold -r 08_referenceLF/scaffold/Mzeb.txt &> log/09_graphs_Mzeb_scaffold &
nice src/02_runMakeGraphs -d 09_variantsLF -t contig -r 08_referenceLF/contig/Bcon.txt &> log/09_graphs_Bcon_contig &
nice src/02_runMakeGraphs -d 09_variantsLF -t contig -r 08_referenceLF/contig/Mzeb.txt &> log/09_graphs_Mzeb_contig &
```

### Faceted graphs

```shell
for species in Bcon Mzeb; do
	for varT in SNP INDEL; do
		for seqT in contig scaffold; do
			echo $species $varT $seqT
			src/02_makeGraphsFacet.R -i 09_variantsLF/$seqT/${species}.txt.xz -o graphs/09_variantsLF/$seqT/$species --selectType $varT --imgTheme "theme_bw" --histFill "#333333" --alpha "0.5" --imgType pdf --imgPrefix $varT
		done
	done
done
```

Graphs grouped by species take too much space. It might be better to combine both species into a single faceted graph.

```shell
xzcat 09_variantsLF/contig/{Bcon,Mzeb}.txt.xz | sed -ne '1 p' -e '/^CHROM/!p' | xz -c > 09_variantsLF/contig/All.txt.xz
xzcat 09_variantsLF/scaffold/{Bcon,Mzeb}.txt.xz | sed -ne '1 p' -e '/^CHROM/!p' | xz -c > 09_variantsLF/scaffold/All.txt.xz
for varT in SNP INDEL; do
	for seqT in contig scaffold; do
		echo $varT $seqT
		src/02_makeGraphsFacet.R -i 09_variantsLF/$seqT/All.txt.xz -o graphs/09_variantsLF/$seqT/All --selectType $varT --imgTheme "theme_bw" --histFill "#333333" --alpha "0.5" --imgType pdf --imgPrefix $varT --imgWidth=16.9 --imgHeight=20.3
	done
done
rm 09_variantsLF/{contig,scaffold}/All.txt.xz
```



## 10. Align resequencing reads

Align actual reads from another _A. thaliana_ genotype to Sim assemblies and Chr1+Chr2 of the reference genome and compare the variants and origin counts at the position `k`.

### Download data

We will use Bs-1 (Basel) genotype resequenced as part of the 1001 genomes initiative ([[200 genomes from Ecker lab|http://trace.ddbj.nig.ac.jp/DRASearch/submission?acc=SRA012474]]) and deposited to [[DDBJ|http://trace.ddbj.nig.ac.jp/DRASearch/experiment?acc=SRX144851]]. The reads were sequenced with Illumina HiSeq 2000 and have the insert size of 202 with 101 read length. There are 167,330,816 read pairs.

BWA does not support bzip.

```shell
mkdir 10_simReseq/data
cd 10_simReseq/data
wget ftp://ftp.ddbj.nig.ac.jp/ddbj_database/dra/fastq/SRA012/SRA012474/SRX144851/SRR492224_1.fastq.bz2
wget ftp://ftp.ddbj.nig.ac.jp/ddbj_database/dra/fastq/SRA012/SRA012474/SRX144851/SRR492224_2.fastq.bz2
for i in 10_simReseq/data/*.bz2; echo $i; bunzip2 $i; gzip ${i%.bz2}; done
```

### Check the quality.

```shell
module load seq/fastqc/0.11.4
sbatch -o log/10_fastqc -t 0:30:0 src/10_jobFastQC
```

### Trim

The quality gets quite bad towards the end. Trimming might be beneficial.

```shell
module load seq/trimmomatic/0.33
sbatch -o log/10_trim -t 2:00:0 src/10_jobTrim
```

### Call variants

```shell
module load seq/bwa/0.7.12 seq/queue/3.4-0 seq/samtools/1.2 seq/picard/1.140

nice src/02_runDetectSlurm contig 05_simReference 10_simReseq 04_simAssembly/Sim_list.txt 10_simReseq/trim/SRR492224-R1.fastq.gz 10_simReseq/trim/SRR492224-R2.fastq.gz 16 16 "-run " &> log/10_detect_contig &

nice src/02_runDetectSlurm scaffold 05_simReference 10_simReseq 04_simAssembly/Sim_list.txt 10_simReseq/trim/SRR492224-R1.fastq.gz 10_simReseq/trim/SRR492224-R2.fastq.gz 16 16 "-run " &> log/10_detect_scaffold &
```

### Generate graphs

```shell
nice src/02_runMakeGraphs -d 10_simReseq -t contig -r 04_simAssembly/Sim_list.txt &> log/10_graphs_contig &
nice src/02_runMakeGraphs -d 10_simReseq -t scaffold -r 04_simAssembly/Sim_list.txt &> log/10_graphs_scaffold &
```

### Transform coordinates

Transform scaffold coordinates into contig coordinates and make graphs.

```shell
src/06_runTransform -d 10_simReseq -r 04_simAssembly/Sim_list_transf.txt -t 200
src/02_runMakeGraphs -d 10_simReseq -t transf -r 04_simAssembly/Sim_list_transf.txt -v filtered.txt | tee log/10_graph_transf
```

Create a position map for all variants, so that we can calculate intersections better.

```shell
sed -nr '/^>/ s/^>([0-9]+) length ([0-9]+).*/\1\t\2/p' 04_simAssembly/soap_K69/SimAtha_K69.contig >> 10_simReseq/contig/Sim_soap_K69/SeqLength.txt
src/06_runTransform -d 10_simReseq -r 04_simAssembly/Sim_list_transf.txt -m transf_all
```

Combine the position maps so that we could add scaffold information to the transf and transf_all variants. The second sed removes repeated headers if they are present.

```shell
for i in transf transf_all; do
	if [[ -f 10_simReseq/$i/PositionMap.txt ]]; then rm 10_simReseq/$i/PositionMap.txt; fi
	for j in `cat 04_simAssembly/Sim_list_transf.txt`; do
		sed -re '1 s/^/RefName\t/' -e '2,$ s/^/'$j'\t/' 10_simReseq/$i/$j/PositionMap.txt >> 10_simReseq/$i/PositionMap.txt
	done
	sed -n -i -e '1 p' -e '/RefName/!p' 10_simReseq/$i/PositionMap.txt
done
```

Find intersections among contig, transf, transf_all, and scaffold.

```shell
src/06_findIntersect.R -d 10_simReseq -o 10_simReseq/intersect.txt -t 'SNP' --thresGQ=40
```


### Combine data

Combine the data to facilitate the generation of faceted graphs.

```shell
for seqT in contig scaffold transf transf_all; do
	if [[ $seqT == @(transf|transf_all) ]]; then sfx="_transf"; else sfx=""; fi
	src/02_tidyData.R -p 10_simReseq/$seqT -r 04_simAssembly/Sim_list${sfx}.txt --fnVariants=filtered.txt --fnSeqLen=SeqLength.txt -o 10_simReseq/$seqT/variants.txt | tee log/10_tidy_$seqT
done
```

In case of scaffolds and transf scaffolds, the maximum number of variants are not at 96 (15 variants) but rather at 1892 (23 variants). Overall, there are 135 positions where the number of variants is 15 or greater. Position 96 seems to be a local maximum though. The other positions are probably artefacts. Since we are interested in position 96 specifically, we need to update the combined data set.

```shell
perl -i'.allp' -nale 'if ($F[12] eq "Sim_allp") { if ($F[1] == 96 || $F[14] == 96) { $F[16] = 1 } else { $F[16] = 0 } } print join("\t", @F)' 10_simReseq/scaffold/variants.txt
perl -i'.allp' -nale 'if ($F[12] eq "Sim_allp") { if ($F[1] == 96 || $F[14] == 96) { $F[16] = 1 } else { $F[16] = 0 } } print join("\t", @F)' 10_simReseq/transf/variants.txt
perl -i'.allp' -nale 'if ($F[12] eq "Sim_allp") { if ($F[1] == 96 || $F[14] == 96) { $F[16] = 1 } else { $F[16] = 0 } } print join("\t", @F)' 10_simReseq/transf_all/variants.txt
```

### Faceted graphs

```shell
for seqT in contig scaffold; do
	for varT in SNP INDEL; do
		echo $seqT $varT
		src/02_makeGraphsFacet.R -i 10_simReseq/$seqT/variants.txt -o graphs/10_simReseq/$seqT/All --posRangeStr 'c(20,120)' --selectType $varT --imgTheme "theme_bw" --histFill "#333333" --alpha "0.5" --imgType pdf --imgPrefix $varT --imgWidth 16.9 --imgHeight 16.9 --panelOrder "2,4,3,5,1"
	done
done
```

Graphs for the transf data set only have two panels, so they need reduced height.

```shell
for seqT in transf transf_all; do
	for varT in SNP INDEL; do
		echo $seqT $varT
		src/02_makeGraphsFacet.R -i 10_simReseq/$seqT/variants.txt -o graphs/10_simReseq/$seqT/All --posRangeStr 'c(20,120)' --selectType $varT --imgTheme "theme_bw" --histFill "#333333" --alpha "0.5" --imgType pdf --imgPrefix $varT --imgWidth 16.9 --imgHeight 8.45
	done
done
```

### Graph intersections

Graph the intersection between contig and transf scaffold variants.

```shell
src/06_runGraphVarIntersect -a 10_simReseq/contig -l "Contig Only" -b 10_simReseq/transf -m "Scaffold (Transformed) Only" -r 04_simAssembly/Sim_list_transf.txt -o graphs/10_simReseq/contig_transf | tee log/10_graphIntersectCS
```

Graph the intersection between the variants from Bs-1 alignments and Sim alignments.

```shell
src/06_runGraphVarIntersect -a 06_simVariants/contig -l "Sim Only" -b 10_simReseq/contig -m "Bs-1 Only" -r 04_simAssembly/Sim_list.txt -o graphs/10_simReseq/sim_reseq_contig | tee log/10_graphIntersectSRC

src/06_runGraphVarIntersect -a 06_simVariants/transf -l "Sim Only" -b 10_simReseq/transf -m "Bs-1 Only" -r 04_simAssembly/Sim_list_transf.txt -o graphs/10_simReseq/sim_reseq_transf | tee log/10_graphIntersectSRM
```

Venn diagrams

```shell
for r in `cat 04_simAssembly/Sim_list.txt`; do
	src/06_graphVarVenn.R -a 06_simVariants/contig/variants.txt --labelA "Simulated" -b 10_simReseq/contig/variants.txt --labelB "Bs-1" -o graphs/10_simReseq/sim_reseq_contig/$r/bw_venn_SNP.pdf -t SNP --refName $r
done

for r in `cat 04_simAssembly/Sim_list_transf.txt`; do
	src/06_graphVarVenn.R -a 06_simVariants/transf/variants.txt --labelA "Simulated" -b 10_simReseq/transf/variants.txt --labelB "Bs-1" -o graphs/10_simReseq/sim_reseq_transf/$r/bw_venn_SNP.pdf -t SNP --refName $r
done
```


## 11. Investigate Bs-1 read alignments

### Extract subsets

Extract sequences that have SNPs at position `k`.

```shell
for i in transf contig; do for j in Sim_allp Sim_soap_K69 Sim_soap_K71; do mkdir -p 11_reseqLoci/$i/$j; done; done
src/07_selectPosVariants.pl -i <(gunzip -c 10_simReseq/contig/Sim_allp/filtered.vcf.gz) -o 11_reseqLoci/contig/Sim_allp/variants.txt -p 96
src/07_selectPosVariants.pl -i <(gunzip -c 10_simReseq/contig/Sim_soap_K69/filtered.vcf.gz) -o 11_reseqLoci/contig/Sim_soap_K69/variants.txt -p 70
src/07_selectPosVariants.pl -i <(gunzip -c 10_simReseq/contig/Sim_soap_K71/filtered.vcf.gz) -o 11_reseqLoci/contig/Sim_soap_K71/variants.txt -p 72
```

Retrieved 168, 5150, and 5155 polymorphisms respectively.

### Retrieve read info

```shell
module load java/jdk/8/40 seq/htsjdk/1.140 dev/scala/2.11.6
JAVA_OPTS=-Xmx32G scala -classpath "$htsjdk/*" src/ExtractReadInfo.scala 11_reseqLoci/contig/Sim_allp/variants.txt 10_simReseq/contig/Sim_allp/duplicatesMarked.bam 10_simReseq/scaffold/Atha/duplicatesMarked.bam 11_reseqLoci/contig/Sim_allp/readInfo.txt
JAVA_OPTS=-Xmx32G scala -classpath "$htsjdk/*" src/ExtractReadInfo.scala 11_reseqLoci/contig/Sim_soap_K69/variants.txt 10_simReseq/contig/Sim_soap_K69/duplicatesMarked.bam 10_simReseq/scaffold/Atha/duplicatesMarked.bam 11_reseqLoci/contig/Sim_soap_K69/readInfo.txt
JAVA_OPTS=-Xmx32G scala -classpath "$htsjdk/*" src/ExtractReadInfo.scala 11_reseqLoci/contig/Sim_soap_K71/variants.txt 10_simReseq/contig/Sim_soap_K71/duplicatesMarked.bam 10_simReseq/scaffold/Atha/duplicatesMarked.bam 11_reseqLoci/contig/Sim_soap_K71/readInfo.txt
```

### Origin Counts

Create a histogram showing origin counts per variant.

```shell
src/07_graphOriginCntHist.R -i 11_reseqLoci/contig/Sim_allp/readInfo.txt -o graphs/11_reseqLoci/contig/originCntHist_Sim_allp.svg --imgTheme=theme_black --colour="#CC8000"
src/07_graphOriginCntHist.R -i 11_reseqLoci/contig/Sim_soap_K69/readInfo.txt -o graphs/11_reseqLoci/contig/originCntHist_Sim_soap_K69.svg --imgTheme=theme_black --colour="#CC8000"
src/07_graphOriginCntHist.R -i 11_reseqLoci/contig/Sim_soap_K71/readInfo.txt -o graphs/11_reseqLoci/contig/originCntHist_Sim_soap_K71.svg --imgTheme=theme_black --colour="#CC8000"
```



## 12. Repetitive Elements

Check if the majority of variants in the `k` position are in repetitive elements. We use 20140131 version of the RepeatMasker library, ncbi engine, and `viridiplantae` species filter.

```shell
module load bio/RepeatMasker/4.0.5
qsub -pe smp 8 -q GT -l h='fgcz-c-047' -l R=8 -v SGE_RESOURCES_RAM=64.0 -l C=1 -v SGE_RESOURCES_CORES=8 -cwd -o log/12_te_sim_contig -j y src/12_jobs/sim_contig
qsub -pe smp 8 -q GT -l h='fgcz-c-047' -l R=8 -v SGE_RESOURCES_RAM=64.0 -l C=1 -v SGE_RESOURCES_CORES=8 -cwd -o log/12_te_sim_scaffold -j y src/12_jobs/sim_scaffold
```

Append nearest repetitive element information to the variants file.

```shell
src/12_addTeToVariants.R -v 06_simVariants/contig/variants.txt -d 12_te/sim/contig
src/12_addTeToVariants.R -v 06_simVariants/scaffold/variants.txt -d 12_te/sim/scaffold
src/12_addTeToVariants.R -v 06_simVariants/transf/variants.txt -d 12_te/sim/contig --fnout variants_transf.txt
src/12_addTeToVariants.R -v 10_simReseq/contig/variants.txt -d 12_te/sim/contig --fnout variants_reseq.txt
src/12_addTeToVariants.R -v 10_simReseq/scaffold/variants.txt -d 12_te/sim/scaffold --fnout variants_reseq.txt
src/12_addTeToVariants.R -v 10_simReseq/transf/variants.txt -d 12_te/sim/contig --fnout variants_reseq_transf.txt
find 12_te -name "*.txt" -exec xz -T 8 {} \;
```

### Faceted graphs

```shell
src/12_graphRepeatVariants.R -i 12_te/sim/contig/variants.txt.xz -o graphs/12_te/sim/contig -t SNP --panelOrder "2,4,3,5,1" --imgThemeStr theme_bw
src/12_graphRepeatVariants.R -i 12_te/sim/scaffold/variants.txt.xz -o graphs/12_te/sim/scaffold -t SNP --panelOrder "2,4,3,5,1" --imgThemeStr theme_bw
src/12_graphRepeatVariants.R -i 12_te/sim/contig/variants_transf.txt.xz -o graphs/12_te/sim/transf -t SNP --imgThemeStr theme_bw --imgHeight 8.45
src/12_graphRepeatVariants.R -i 12_te/sim/contig/variants_reseq.txt.xz -o graphs/12_te/reseq/contig -t SNP --panelOrder "2,4,3,5,1" --imgThemeStr theme_bw
src/12_graphRepeatVariants.R -i 12_te/sim/scaffold/variants_reseq.txt.xz -o graphs/12_te/reseq/scaffold -t SNP --panelOrder "2,4,3,5,1" --imgThemeStr theme_bw
src/12_graphRepeatVariants.R -i 12_te/sim/contig/variants_reseq_transf.txt.xz -o graphs/12_te/reseq/transf -t SNP --imgThemeStr theme_bw --imgHeight 8.45
```

### Frequency and proportion of SNPs by TE family

Collect information about the families. Symlinking below is done to simplify the data processing.

```shell
src/12_summariseFamilies.R -p 12_te/sim/contig -r 04_simAssembly/Sim_list.txt -o 12_te/sim/contig/TeFamilies.txt
src/12_summariseFamilies.R -p 12_te/sim/scaffold -r 04_simAssembly/Sim_list.txt -o 12_te/sim/scaffold/TeFamilies.txt
for a in `cat 04_simAssembly/Sim_list.txt`; do ln -s ../../sim/contig/$a 12_te/reseq/contig; done
for a in `cat 04_simAssembly/Sim_list.txt`; do ln -s ../../sim/scaffold/$a 12_te/reseq/scaffold; done
src/12_summariseFamilies.R -p 12_te/reseq/contig -r 04_simAssembly/Sim_list.txt -o 12_te/reseq/contig/TeFamilies.txt
src/12_summariseFamilies.R -p 12_te/reseq/scaffold -r 04_simAssembly/Sim_list.txt -o 12_te/reseq/scaffold/TeFamilies.txt
```

If a repetitive element spans position `k` at both end of a sequence, it is counted as a single occurrence. That is why the proportion is calculated using the number of sequences rather than the number of bases. 

```shell
src/12_graphVarFreqByTeFamily.R -i 12_te/sim/contig/variants.txt.xz -f 12_te/sim/contig/TeFamilies.txt -o graphs/12_te/sim/contig/TeFamilies.pdf --imgThemeStr=theme_bw
src/12_graphVarFreqByTeFamily.R -i 12_te/sim/scaffold/variants.txt.xz -f 12_te/sim/scaffold/TeFamilies.txt -o graphs/12_te/sim/scaffold/TeFamilies.pdf --imgThemeStr=theme_bw
src/12_graphVarFreqByTeFamily.R -i 12_te/sim/contig/variants_reseq.txt.xz -f 12_te/reseq/contig/TeFamilies.txt -o graphs/12_te/reseq/contig/TeFamilies.pdf --imgThemeStr=theme_bw
src/12_graphVarFreqByTeFamily.R -i 12_te/sim/scaffold/variants_reseq.txt.xz -f 12_te/reseq/scaffold/TeFamilies.txt -o graphs/12_te/reseq/scaffold/TeFamilies.pdf --imgThemeStr=theme_bw
```



## 13. Filter repetitive elements

Filter variants that are in repetitive elements.

```shell
src/13_filterTeVariants.R -v 12_te/sim/contig/variants.txt -o 13_teFilt/sim/contig/variants.txt
src/13_filterTeVariants.R -v 12_te/sim/contig/variants_transf.txt -o 13_teFilt/sim/contig/variants_transf.txt
src/13_filterTeVariants.R -v 12_te/sim/contig/variants_reseq_transf.txt -o 13_teFilt/sim/contig/variants_reseq_transf.txt
src/13_filterTeVariants.R -v 12_te/sim/scaffold/variants.txt -o 13_teFilt/sim/scaffold/variants.txt
src/13_filterTeVariants.R -v 12_te/sim/scaffold/variants_reseq.txt -o 13_teFilt/sim/scaffold/variants_reseq.txt
```

Graph intersection

```shell
src/06_graphVarIntersect.R -a 13_teFilt/sim/scaffold/variants.txt --labelA "Sim Only" -b 13_teFilt/sim/scaffold/variants_reseq.txt --labelB "Bs-1 Only" -o graphs/13_teFilt/sim/scaffold_reseq/Sim_allp --refName Sim_allp --selectType SNP --posRangeStr "c(20, 120)" --imgType pdf --imgTheme theme_bw --histFill "#333333" --imgHeight 20.3 --imgWidth 16.9

src/06_graphVarIntersect.R -a 13_teFilt/sim/scaffold/variants.txt --labelA "Sim Only" -b 13_teFilt/sim/scaffold/variants_reseq.txt --labelB "Bs-1 Only" -o graphs/13_teFilt/sim/scaffold_reseq/Sim_sga_m75 --refName Sim_sga_m75 --selectType SNP --posRangeStr "c(20, 120)" --imgType pdf --imgTheme theme_bw --histFill "#333333" --imgHeight 20.3 --imgWidth 16.9

src/06_graphVarIntersect.R -a 13_teFilt/sim/scaffold/variants.txt --labelA "Sim Only" -b 13_teFilt/sim/scaffold/variants_reseq.txt --labelB "Bs-1 Only" -o graphs/13_teFilt/sim/scaffold_reseq/Sim_soap_K69 --refName Sim_soap_K69 --selectType SNP --posRangeStr "c(20, 120)" --imgType pdf --imgTheme theme_bw --histFill "#333333" --imgHeight 20.3 --imgWidth 16.9

src/06_graphVarIntersect.R -a 13_teFilt/sim/contig/variants_transf.txt --labelA "Sim Only" -b 13_teFilt/sim/contig/variants_reseq_transf.txt --labelB "Bs-1 Only" -o graphs/13_teFilt/sim/transf_reseq/Sim_allp --refName Sim_allp --selectType SNP --posRangeStr "c(20, 120)" --imgType pdf --imgTheme theme_bw --histFill "#333333" --imgHeight 20.3 --imgWidth 16.9

src/06_graphVarIntersect.R -a 13_teFilt/sim/contig/variants_transf.txt --labelA "Sim Only" -b 13_teFilt/sim/contig/variants_reseq_transf.txt --labelB "Bs-1 Only" -o graphs/13_teFilt/sim/transf_reseq/Sim_soap_K69 --refName Sim_soap_K69 --selectType SNP --posRangeStr "c(20, 120)" --imgType pdf --imgTheme theme_bw --histFill "#333333" --imgHeight 20.3 --imgWidth 16.9
```

Venn diagrams

```shell
for r in `cat 04_simAssembly/Sim_list.txt`; do
	src/06_graphVarVenn.R -a 13_teFilt/sim/scaffold/variants.txt --labelA "Simulated" -b 13_teFilt/sim/scaffold/variants_reseq.txt --labelB "Bs-1" -o graphs/13_teFilt/sim/scaffold_reseq/$r/bw_venn_SNP.pdf -t SNP --refName $r
done

for r in `cat 04_simAssembly/Sim_list_transf.txt`; do
	src/06_graphVarVenn.R -a 13_teFilt/sim/contig/variants_transf.txt --labelA "Simulated" -b 13_teFilt/sim/contig/variants_reseq_transf.txt --labelB "Bs-1" -o graphs/13_teFilt/sim/transf_reseq/$r/bw_venn_SNP.pdf -t SNP --refName $r
done
```



## 14. Run variant calling pipeline using Bowtie2

### Create Bowtie2 index

```shell
module load seq/bowtie2/2.3.0
for a in `cat 04_simAssembly/Sim_list.txt`; do echo $a; bowtie2-build --threads 8 05_simReference/contig/${a}.fa 05_simReference/contig/${a}.fa; done
```

### Detect variants

The `14_runDetectBowtie2` script is deprecated as of 2017-02-01. You should use the `--aligner bowtie2` additional parameter with the `02_runDetect` script instead. See step 18 for details.

```shell
module load seq/bowtie2/2.3.0 seq/queue/3.4-0 seq/samtools/1.2 seq/picard/1.130
nice src/14_runDetectBowtie2 contig 05_simReference 14_simVarBowtie 04_simAssembly/Sim_list.txt 03_simAtha/Atha_200_R1.fastq.gz 03_simAtha/Atha_200_R2.fastq.gz 16 16 "-run " &> log/14_simVarBowtie_contig &
```

### Combine data

Combine the data to facilitate the generation of faceted graphs. Since the highest count is not always at position `k`, we have to override the guesses (`--expectedk` parameter).

```shell
module load seq/samtools/1.2
seqT=contig
for a in `cat 04_simAssembly/Sim_list.txt`; do d=14_simVarBowtie/$seqT/$a; f=$d/SeqLength.txt; echo -e "Contig\tLength" > $f; samtools idxstats $d/duplicatesMarked.bam | cut -f 1,2 | grep -v '^*' >> $f; done
src/02_tidyData.R -p 14_simVarBowtie/$seqT -r 04_simAssembly/Sim_list.txt --fnVariants=filtered.txt --fnSeqLen=SeqLength.txt -o 14_simVarBowtie/$seqT/variants.txt --expectedk=src/06_expected_k.txt | tee log/14_tidy_$seqT
```

### Generate graphs

```shell
seqT=contig
varT=SNP
src/02_makeGraphsFacet.R -i 14_simVarBowtie/$seqT/variants.txt -o graphs/14_simVarBowtie/$seqT/All --posRangeStr 'c(20,120)' --selectType $varT --imgTheme "theme_bw" --histFill "#333333" --alpha "0.5" --imgType pdf --imgPrefix $varT --imgWidth 16.9 --imgHeight 16.9 --panelOrder "2,4,3,5,1"
```



## 15. Call variants using FreeBayes

In the script below, GATK is only used to extract variant information from the filtered VCF file.

### Detect variants

```shell
module load seq/freebayes/1.1.0 seq/gatk/3.4-0
pyenv shell 2.7.12
nice src/15_runFreeBayes contig 05_simReference 06_simVariants 15_simVarFreeBayes 04_simAssembly/Sim_list.txt 200 8 7000000 >& log/15_simVarFreeBayes.log &
seqT=contig
varT=SNP
src/02_makeGraphsFacet.R -i 15_simVarFreeBayes/$seqT/variantsFilt.txt -o graphs/15_simVarFreeBayes/$seqT/All --posRangeStr 'c(20,120)' --selectType $varT --imgTheme "theme_bw" --histFill "#333333" --alpha "0.5" --imgType pdf --imgPrefix $varT --imgWidth 16.9 --imgHeight 16.9 --panelOrder "2,4,3,5,1"
```

### Combine data

Combine the data to facilitate the generation of faceted graphs.

```shell
module load seq/samtools/1.2
seqT=contig
for a in `cat 04_simAssembly/Sim_list.txt`; do ln -s ../../../14_simVarBowtie/$seqT/$a/SeqLength.txt 15_simVarFreeBayes/$seqT/$a; done
src/15_tidyData.R -p 15_simVarFreeBayes/$seqT -r 04_simAssembly/Sim_list.txt --fnVariants=variantsFilt.txt --fnSeqLen=SeqLength.txt -o 15_simVarFreeBayes/$seqT/variants.txt | tee log/15_tidy_$seqT
```

### Generate graphs

```shell
seqT=contig
varT=SNP
src/15_makeGraphsFacet.R -i 15_simVarFreeBayes/$seqT/variants.txt -o graphs/15_simVarFreeBayes/$seqT/All --posRangeStr 'c(20,120)' --selectType $varT --imgTheme "theme_bw" --histFill "#333333" --imgType pdf --imgPrefix $varT --imgWidth 16.9 --imgHeight 16.9 --panelOrder "2,4,3,5,1"
```



## 16. Call variants using Samtools mpileup

### Call variants

```shell
module load seq/samtools/1.2 seq/bcftools/1.2
nice src/16_runMpileup contig 05_simReference 06_simVariants 16_simVarMpileup 04_simAssembly/Sim_list.txt 200 >& log/16_simVarMpileup.log &
```

### Combine data

Combine the data to facilitate the generation of faceted graphs.

```shell
module load seq/samtools/1.2
seqT=contig
for a in `cat 04_simAssembly/Sim_list.txt`; do ln -s ../../../14_simVarBowtie/$seqT/$a/SeqLength.txt 16_simVarMpileup/$seqT/$a; done
src/15_tidyData.R -p 16_simVarMpileup/$seqT -r 04_simAssembly/Sim_list.txt --fnVariants=variantsFilt.txt --fnSeqLen=SeqLength.txt -o 16_simVarMpileup/$seqT/variants.txt | tee log/16_tidy_$seqT
```

### Generate graphs

```shell
seqT=contig
varT=SNP
src/15_makeGraphsFacet.R -i 16_simVarMpileup/$seqT/variants.txt -o graphs/16_simVarMpileup/$seqT/All --posRangeStr 'c(20,120)' --selectType $varT --imgTheme "theme_bw" --histFill "#333333" --imgType pdf --imgPrefix $varT --imgWidth 16.9 --imgHeight 16.9 --panelOrder "2,4,3,5,1"
```



## 17. Align Bs-1 reads with Bowtie2

### Call variants

```shell
module load seq/bowtie2/2.3.0 seq/queue/3.4-0 seq/samtools/1.2 seq/picard/1.130

nice src/14_runDetectBowtie2 contig 05_simReference 17_simReseqBowtie 04_simAssembly/Sim_list.txt 10_simReseq/trim/SRR492224-R1.fastq.gz 10_simReseq/trim/SRR492224-R2.fastq.gz 16 16 "-run " &> log/17_detect_contig &
```

### Combine data

Combine the data to facilitate the generation of faceted graphs. Since the highest count is not always at position `k`, we have to override the guesses (`--expectedk` parameter).

```shell
module load seq/samtools/1.2
seqT=contig
for a in `cat 04_simAssembly/Sim_list.txt`; do ln -s ../../../14_simVarBowtie/$seqT/$a/SeqLength.txt 17_simReseqBowtie/$seqT/$a; done
src/02_tidyData.R -p 17_simReseqBowtie/$seqT -r 04_simAssembly/Sim_list.txt --fnVariants=filtered.txt --fnSeqLen=SeqLength.txt -o 17_simReseqBowtie/$seqT/variants.txt --expectedk=src/06_expected_k.txt | tee log/17_tidy_$seqT
```

### Generate graphs

```shell
seqT=contig
varT=SNP
src/02_makeGraphsFacet.R -i 17_simReseqBowtie/$seqT/variants.txt -o graphs/17_simReseqBowtie/$seqT/All --posRangeStr 'c(20,120)' --selectType $varT --imgTheme "theme_bw" --histFill "#333333" --alpha "0.5" --imgType pdf --imgPrefix $varT --imgWidth 16.9 --imgHeight 16.9 --panelOrder "2,4,3,5,1"
```

### Add TE information

```shell
src/12_addTeToVariants.R -v 17_simReseqBowtie/contig/variants.txt -d 12_te/sim/contig --fnout variants_reseq_bowtie.txt
mv 12_te/sim/contig/variants_reseq_bowtie.txt 17_simReseqBowtie/contig/variants_te.txt
xz 17_simReseqBowtie/contig/variants_te.txt
```



## 18. Run variant calling pipeline using NextGenMap

### Detect variants

NextGenMap creates the indices automatically.

```shell
module load seq/NextGenMap/0.5.0 seq/queue/3.4-0 seq/samtools/1.2 seq/picard/1.130
nice src/02_runDetect contig 05_simReference 18_simVarNgm 04_simAssembly/Sim_list.txt 03_simAtha/Atha_200_R1.fastq.gz 03_simAtha/Atha_200_R2.fastq.gz 8 16 "--aligner ngm -run " &> log/18_simVarNgm_contig &
```

### Combine data

Combine the data to facilitate the generation of faceted graphs.

```shell
module load seq/samtools/1.2
seqT=contig
for a in `cat 04_simAssembly/Sim_list.txt`; do ln -s ../../../14_simVarBowtie/$seqT/$a/SeqLength.txt 18_simVarNgm/$seqT/$a; done
src/02_tidyData.R -p 18_simVarNgm/$seqT -r 04_simAssembly/Sim_list.txt --fnVariants=filtered.txt --fnSeqLen=SeqLength.txt -o 18_simVarNgm/$seqT/variants.txt | tee log/18_tidy_$seqT
```

### Generate graphs

```shell
seqT=contig
varT=SNP
src/02_makeGraphsFacet.R -i 18_simVarNgm/$seqT/variants.txt -o graphs/18_simVarNgm/$seqT/All --posRangeStr 'c(20,120)' --selectType $varT --imgTheme "theme_bw" --histFill "#333333" --alpha "0.5" --imgType pdf --imgPrefix $varT --imgWidth 16.9 --imgHeight 16.9 --panelOrder "2,4,3,5,1"
```


## 19. Run variant calling pipeline using GSNAP

### Detect variants

The index is built by the `02_detect.scala` script but we should probably remove it at the end as it is huge.

```shell
module load seq/gsnap/2016-11-07 seq/queue/3.4-0 seq/samtools/1.2 seq/picard/1.130
mkdir -p 19_simVarGsnap/contig/gmapdb
nice src/02_runDetect contig 05_simReference 19_simVarGsnap 04_simAssembly/Sim_list.txt 03_simAtha/Atha_200_R1.fastq.gz 03_simAtha/Atha_200_R2.fastq.gz 8 16 "--aligner gsnap --gsnapD 19_simVarGsnap/contig/gmapdb -run " &> log/19_simVarGsnap_contig &
```

### Combine data

Combine the data to facilitate the generation of faceted graphs.

```shell
module load seq/samtools/1.2
seqT=contig
for a in `cat 04_simAssembly/Sim_list.txt`; do ln -s ../../../14_simVarBowtie/$seqT/$a/SeqLength.txt 19_simVarGsnap/$seqT/$a; done
src/02_tidyData.R -p 19_simVarGsnap/$seqT -r 04_simAssembly/Sim_list.txt --fnVariants=filtered.txt --fnSeqLen=SeqLength.txt -o 19_simVarGsnap/$seqT/variants.txt | tee log/19_tidy_$seqT
```

### Generate graphs

```shell
seqT=contig
varT=SNP
src/02_makeGraphsFacet.R -i 19_simVarGsnap/$seqT/variants.txt -o graphs/19_simVarGsnap/$seqT/All --posRangeStr 'c(20,120)' --selectType $varT --imgTheme "theme_bw" --histFill "#333333" --alpha "0.5" --imgType pdf --imgPrefix $varT --imgWidth 16.9 --imgHeight 16.9 --panelOrder "2,4,3,5,1"
```

