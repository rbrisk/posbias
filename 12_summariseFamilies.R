#!/usr/bin/env Rscript

require(optparse, quietly=T)
require(futile.logger, quietly=T)

optList <- list(
		make_option(c("-p", "--prefix"), type = "character",
				help = "Data directory prefix"),
		make_option(c("-r", "--refNames"), type = "character",
				help = "File with reference (team) names that should match subdirectory names."),
		make_option(c("-f", "--fileName"), type = "character", default = "RepeatMasker.gff",
				help = "Name of the GFF file that contains TE annotation. Default: '%default'"),
		make_option(c("-o", "--pathOut"), type = "character",
				help = "Output file"),
		make_option(c("-l", "--verbosity"), type = "integer", default = INFO,
				help = "Logging verbosity (t=9,d=8,i=6,w=4,e=2,f=1). Default: %default")
	)


opt <- parse_args( OptionParser(
		usage = "Usage: %prog [options]",
		option_list = optList,
		description = "For each specified reference, calculates the total sequence length and count per TE family.") )


require(dplyr, quietly=T, warn.conflicts=F)
require(readr, quietly=T)

devnull <- flog.threshold(opt$verbosity)
gffColTypes <- "ccciiiccc"
familyPtrn <- '^.*family=([^;/?]+).*$'

dirOut <- dirname(opt$pathOut)
if ( ! file.exists(dirOut) ) {
	dir.create(dirOut, recursive=T);
}

flog.info("Loading the data")
refNames <- scan(opt$refNames, character(), quiet=T)
allTeList <- list()
for (r in refNames) { 
	flog.debug(r)
	pathIn <- file.path(opt$prefix, r, opt$fileName)
	allTeList[[r]] <- read_tsv(pathIn, comment = "#", col_names = F, col_types = "ccciiiccc") %>% 
			transmute(RefName = r, Family = sub(familyPtrn, '\\1', X9), Length = X5 - X4) 
}

flog.info("Combining the data")
allTe <- bind_rows(allTeList) %>% 
		group_by(RefName, Family) %>% 
		summarise(TotalLength = sum(Length), Count = n())

flog.info("Writing results")
write_tsv(allTe, opt$pathOut)

flog.info("Done")
