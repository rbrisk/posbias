#!/usr/bin/env perl

use strict;
use warnings;
use Getopt::Long;
use Pod::Usage;
use Set::Scalar;

my $man = 0;
my $help = 0;
my $skipTerminal = 0;
my $minContigLen = 200;
my $scafTempl = "";
my %scafTemplates = (
		"allp" => "scaffold_%d",
		"soap" => "scaffold%d",
	);
my ($pathIn, $pathOut, $k, $fileType);

GetOptions(
      "h|help" => \$help,
      "man"    => \$man,
      "i=s"    => \$pathIn,
      "o=s"    => \$pathOut,
      "t=s"    => \$fileType,
      "k=i"    => \$k,
		"min-contig=i" => \$minContigLen,
		"skip-terminal" => \$skipTerminal,
		"scaf-templ=s" => \$scafTempl,
   ) or pod2usage(2);

pod2usage(1) if $help;
pod2usage(-verbose => 2) if $man;

die("Input file is required") unless $pathIn;
die("K is required") unless $k;
die("Position '$k' should be positive") unless $k > 0;
die("Minimum contig length '$minContigLen' should be greater than K '$k'.") unless $minContigLen > $k;

sub parseAllp {
	my ($fh, $k) = @_;

	# Skip the comment on top. We assume it is a single paragraph separated from the data by a blank line.
	while (<$fh>) {
		last if $_ =~ /^\s*$/;
	}

	my %scaffolds;
	# We need scaffold length in order to remove K positions if skipTerminal is requested. The last
	# position in the list may not be K position from the 3' end because the last contig may be
	# shorter than minContigLen.
	my %scaffoldLen;
	my $scafId = -1;
	my $cumLen = 0;
	while (<$fh>) {
		chomp;
		if ($_ =~ /^\s*$/) {
			next;
		} elsif ($_ =~ /^scaffold (\d+)/) {
			# Add length of the completed scaffold
			$scaffoldLen{$scafId} = $cumLen if $cumLen > 0;
			# Rotate scaffold info
			$scafId = $1;
			$scaffolds{$scafId} = Set::Scalar->new;
			$cumLen = 0;
		} elsif ($_ =~ /^\d.*\(l = (\d+)\)$/) {
			# Firt contig in the scaffold
			my $contigLen = $1;
			$cumLen += $contigLen;
			if ($contigLen >= $minContigLen) {
				$scaffolds{$scafId}->insert($k);
				$scaffolds{$scafId}->insert($cumLen - $k + 1);
			}
		} elsif ($_ =~ /^ -- \((-?\d+).+l = (\d+)\)$/) {
			# Subsequent contigs; negative gap means overlap
			my $gapLen = $1;
			my $contigLen = $2;
			$cumLen += $gapLen + $contigLen;
			if ($contigLen >= $minContigLen) {
				$scaffolds{$scafId}->insert($cumLen - $contigLen + $k);
				$scaffolds{$scafId}->insert($cumLen - $k + 1);
			}
		}
	}
	# Add length of the last scaffold
	$scaffoldLen{$scafId} = $cumLen;
	return (\%scaffolds, \%scaffoldLen);
}


sub parseSoap {
	my ($fh, $k) = @_;
	
	my %scaffolds;
	my %scaffoldLen;
	my $scafId = -1;
	my $cumLen = 0;
	while (<$fh>) {
		chomp;
		if ($_ =~ /^>scaffold(\d+)/) {
			# Add length of the completed scaffold
			$scaffoldLen{$scafId} = $cumLen if $cumLen > 0;
			# Rotate scaffold info
			$scafId = $1;
			$scaffolds{$scafId} = Set::Scalar->new;
			$cumLen = 0;
		} else {
			# contigBeg has origin 0, that's why 1 is not subtracted when calculating the end position.
			my ($contigId, $contigBeg, $strand, $contigLen) = split(/\s+/, $_);
			$cumLen = $contigBeg + $contigLen;
			if ($contigLen >= $minContigLen) {
				$scaffolds{$scafId}->insert($contigBeg + $k);
				$scaffolds{$scafId}->insert($cumLen - $k + 1);
			}
		}
	}
	# Add length of the last scaffold
	$scaffoldLen{$scafId} = $cumLen;
	return (\%scaffolds, \%scaffoldLen);
}


my %dispatcher = ( 
		allp => \&parseAllp, 
		soap => \&parseSoap,
	);

die("Unsupported file type. Supported types: " . join(", ", keys %dispatcher)) 
		unless defined $dispatcher{$fileType};

open(my $fh, "<", $pathIn) or die("Unable to read the input file '$pathIn'. $!");
my ($scaffolds, $scaffoldLen) = $dispatcher{$fileType}->($fh, $k);
$fh->close();

open(FOUT, "> " . ($pathOut || '-')) or die("Unable to write the output '$pathOut'. $!");
print FOUT join("\t", qw/ Scaffold Pos /), "\n";
$scafTempl = $scafTemplates{$fileType} unless $scafTempl;
for my $scafId (sort { $a <=> $b } keys(%$scaffolds)) {
	my $scafName = sprintf($scafTempl, $scafId);
	my $posSet = $scaffolds->{$scafId};
	if ($skipTerminal) {
		$posSet->delete($k);
		$posSet->delete($scaffoldLen->{$scafId} - $k + 1);
	}
	for my $pos (sort { $a <=> $b } $posSet->elements) {
		print FOUT join("\t", $scafName, $pos), "\n";
	}
}
close(FOUT);


__END__

=encoding utf8

=head1 NAME

10_findContigKinScaf.pl - Finds all positions in scaffolds that correspond to position K in contigs.

=head1 SYNOPSIS

10_findContigKinScaf.pl -i input.txt -t allpaths -k 95 -o positions.txt

=head1 OPTIONS

=over 8

=item B<-i>

Input file that describes contig positions within scaffolds.

=item B<-t>

Input type format. Currently supported formats: allp (allpaths), soap (soapdenovo)

=item B<-k>

Position within contig (both 5' and 3' positions are returned).

=item B<--min-contig>

(Optional) Minimum length of contigs to include. Default: 200

=item B<--skip-terminal>

(Optional) If specified, the terminal positions (K position from each end of a scaffold) will be
excluded.

=item B<--scaf-templ>

(Optional) Template for writing scaffold names. Default: "scaffold_%d" for allp and "scaffold%d" for
soap

=item B<-o>

(Optional) Output file. Tab-delimited file that includes scaffold id and position columns. Default:
STDOUT

=back

=head1 DESCRIPTION

The script finds all positions in scaffolds that correspond to position K in contigs. Both 5' and 3'
ends of each contig are mapped. The output file is guaranteed to have only unique positions.

=head1 AUTHOR

Roman Briskine, Universität Zürich

=cut
