#!/usr/bin/env Rscript

require(optparse, quietly=T)
require(futile.logger, quietly=T)

optList <- list(
		make_option(c("-i", "--variants"), type = "character",
				help = "Path to the tidied variant table with repeat annotation"),
		make_option(c("-f", "--families"), type = "character",
				help = "Path to the file that contains TE families, their counts, and lengths"),
		make_option(c("-o", "--pathOut"), type = "character",
				help = "Output path"),
		make_option(c("-t", "--type"), type = "character",
				help = "(Optional) Variant type filter, e.g. 'SNP' or 'INDEL'"),
		make_option("--maxDist", type = "integer", default = 0,
				help = "(Optional) Maximum distance to repetitive element to be considered within it. Default: %default"),
		make_option("--minSeqLen", type = "integer", default = 1,
				help = "(Optional) Minimum sequence length. Variants on shorter sequences are ignored. Default: %default"),
		make_option("--imgThemeStr", type = "character", default = "theme_black",
				help = "(Optional) ggplot2 theme string. Default: \"%default\""),
		make_option("--imgHeight", type = "double", default = 16.9,
				help = "(Optional) Image height in cm. Default: %default"),
		make_option("--imgWidth", type = "double", default = 16.9,
				help = "(Optional) Image width in cm. Default: %default"),
		make_option(c("-l", "--verbosity"), type = "integer", default = INFO,
				help = "(Optional) Logging verbosity (t=9,d=8,i=6,w=4,e=2,f=1). Default: %default")
	)


opt <- parse_args( OptionParser(
		usage = "Usage: %prog [options]",
		option_list = optList,
		description = "Builds histograms showing the proportion or frequency of SNPs within TE families") )


require(dplyr, quietly=T, warn.conflicts=F)
require(grid, quietly=T)
require(ggplot2, quietly=T)
require(readr, quietly=T)
require(stringr, quietly=T)


varType <- "SNP"
allImgThemes = c("theme_grey", "theme_gray", "theme_bw", "theme_black", "theme_classic", 
		"theme_minimal", "theme_light", "theme_linedraw")
allImgTypes = c("svg", "png", "jpg", "tiff", "pdf")

devnull <- flog.threshold(opt$verbosity)

if (any(allImgThemes == opt$imgThemeStr)) {
	opt$imgTheme <- eval(parse(text=sprintf("%s()", opt$imgThemeStr)))
} else {
	stop("Invalid ggplot theme")
}

if (opt$maxDist < 0) {
	stop("maxDist cannot be negative")
}

dirOut <- dirname(opt$pathOut)
if ( ! file.exists(dirOut) ) {
	dir.create(dirOut, recursive=T);
}

defPalette <- c("#E69F00", "#56B4E9", "#F0E442", "#009E73", "#D55E00", "#0072B2", "#CC79A7")
mkSubPalette <- function(n, plt = defPalette) {
	paletteFunc <- colorRampPalette(defPalette)
	pltN <- length(plt)
	if (n < pltN) {
		paletteFunc(pltN)[1:n]
	} else {
		paletteFunc(n)
	}
}

flog.info("Loading the data")

variants <- read_tsv(opt$variants, col_types="cicdddddddiiciiiici") %>%
		filter(Is.k == 1)
flog.debug("Found %d records", nrow(variants))

# Select requested type unless ALL types are requested
if (! is.null(opt$selectType) ) {
	flog.info("Filtering the data by type")
	variants <- filter(variants, TYPE == opt$selectType)
	flog.debug("After type filter: %d", nrow(variants))
}

# Remove SNPs on contigs that are too small
if (opt$minSeqLen > 1) {
	variants <- filter(variants, SeqLength >= opt$minSeqLen)
	flog.debug("After sequence length filter: %d", nrow(variants))
}

if (nrow(variants) == 0) {
	stop(sprintf("The input table has no records of type %s", opt$selectType)) 
}

variants <- variants %>%
		transmute(RefName = RefName, Family = sub("\\??/.+$", "", TE.Family), CHROM) %>% 
		group_by(RefName, Family) %>% 
		summarise(VarCount = n_distinct(CHROM))

families <- read_tsv(opt$families)

dsJ <- left_join(families, variants, by = c("RefName" = "RefName", "Family" = "Family")) %>% 
		mutate(VarCount = ifelse(is.na(VarCount), 0, VarCount))

ds <- bind_rows(
		transmute(dsJ, RefName, Family, Type = "Count", Value = VarCount),
		transmute(dsJ, RefName, Family, Type = "Proportion", Value = VarCount / Count)
	)

flog.info("Rendering")
gObj <- ggplot(ds, aes(Family, Value)) + 
		opt$imgTheme +
		geom_bar(aes(fill = RefName), position = "dodge", stat = "identity") + 
		scale_fill_manual(values = mkSubPalette(length(unique(ds$RefName))), name = "") + 
		facet_wrap(~ Type, ncol = 1, scales = "free_y", strip.position = "right") + 
		theme(axis.text.x = element_text(angle=45, hjust=1, vjust=1), axis.title = element_blank())
ggsave(opt$pathOut, gObj, width=opt$imgWidth, height=opt$imgHeight, units="cm")

flog.info("Done")
