import htsjdk.samtools.DefaultSAMRecordFactory
import htsjdk.samtools.SAMFileWriter
import htsjdk.samtools.SAMFileWriterFactory
import htsjdk.samtools.SAMRecord
import htsjdk.samtools.SamInputResource
import htsjdk.samtools.SamReader
import htsjdk.samtools.SamReaderFactory
import htsjdk.samtools.ValidationStringency
import htsjdk.samtools.seekablestream.SeekableStream

import java.io.File
import java.io.PrintWriter
import java.io.IOException
import java.net.MalformedURLException
import java.net.URL

import scala.io.Source
import scala.collection.immutable._
import scala.collection.mutable.ListBuffer

object ExtractReadInfo {
	def main(args: Array[String]) {
		if (args.length != 4) {
			println("Usage: scala -classpath \"htsjdk/*\" ExtractReadInfo.scala variants.txt assembly.bam origin.bam output.txt")
			throw new IllegalArgumentException("Exactly four arguments are required")
		}
		val pathVariants  = new File(args(0))
		val pathAlignments = new File(args(1))
		val pathOrigin = new File(args(2))
		val pathOut = new File(args(3))

		val extractor = new ReadInfoExtractor()
		val alnReader: SamReader = SamReaderFactory.makeDefault().open(pathAlignments)
		val variants = extractor.readVariants(pathVariants)
		println(s"Found ${variants.size} variants")
		val variantReads = 
			for { 
				v <- variants
				alignments = extractor.extractAlignments(alnReader, v.seqId, v.pos)
			} yield alignments
		val readNames = getReadNames(variantReads)
		println(s"Extracted ${readNames.size} reads that align to the variants")
		alnReader.close

		val originReader: SamReader = SamReaderFactory.makeDefault().open(pathOrigin)
		val origins = extractor.extractOrigins(originReader, readNames)

		val out = new PrintWriter(pathOut)
		val header = List(
				"SeqId", "Pos", "RefAllele", "VarAllele", 
				"Read", "PosInRead", "ReadAllele", "AlnStart", "MQ", "Cigar", "Strand",
				"MateSeqId", "MateAlnStart", "Insert", "MateMapped", "ProperPair",
				"OrigSeqId", "OrigAlnStart", "OrigInsert", "OrigCigar", "OrigStrand",
				"OrigPos")
		out.println(header.mkString("\t"))
		for (x <- variants.zip(variantReads)) {
			val variantStr = x._1.asString
			val reads = x._2
			for (r <- reads) {
				val rOrigin: Option[SAMRecord] = origins.get(r.readName)
				/*
				val variantOriginPos = 
						if (r.readName.endsWith("1")) rOrigin.readStart + r.posInRead - 1
						else rOrigin.readStart - r.posInRead + 1
				*/
				// If the read aligns to different strands in the assembly and the reference, the
				// position should be specified from the other end, i.e. read_len - pos + 1
				val rOriginList = rOrigin match {
					case Some(ro) => List(
							ro.getContig, 
							ro.getStart.toString, 
							ro.getInferredInsertSize.toString,
							ro.getCigarString,
							if (ro.getReadNegativeStrandFlag) "-" else "+",
							if (r.isNegStrand ^ ro.getReadNegativeStrandFlag)
								ro.getReferencePositionAtReadPosition(ro.getReadLength - r.posInRead + 1)
							else ro.getReferencePositionAtReadPosition(r.posInRead)
						)
					case None => List("", "", "", "", "", "")
				}
				val outList = List(variantStr, r.asString, rOriginList.mkString("\t"))
				out.println(outList.mkString("\t"))
			}
		}
		out.close()
		println("Done")
	}

	def getReadNames(variantReads: List[List[Main.ReadAlignment]]) = {
		val allReads = for {
			readList <- variantReads
			read <- readList
		} yield read.readName
		allReads.toSet
	}

	class ReadInfoExtractor() {

		def readVariants(pathVariants: File) : List[Variant] = {
			for {
				variant <- Source.fromFile(pathVariants).getLines.map(VariantFactory.fromString).toList
				if variant.filt == "PASS"
			} yield variant
		}

			
		def extractAlignments(alnReader: SamReader, seqId: String, pos: Int) : List[ReadAlignment] = {
			var alignments = new ListBuffer[ReadAlignment]
			val readsIter = alnReader.queryOverlapping(seqId, pos, pos)
			while (readsIter.hasNext) {
				val read = readsIter.next
				// Somehow it returns reads that do not overlap the reference position (i.e.
					// getReadPositionAtReferencePosition returns 0
				if ( ! read.getNotPrimaryAlignmentFlag && read.getReadPositionAtReferencePosition(pos) > 0) {
					val readNameSfx = if (read.getFirstOfPairFlag) "1" else "2"
					val posInRead = read.getReadPositionAtReferencePosition(pos)
					val readString = read.getReadString
					val alleleRead = readString(posInRead - 1).toString
					val mateRefName: Option[String] = Option(read.getMateReferenceName)
							
					// We need to append read suffix to the name to make sure we get correct origin read
					alignments += ReadAlignment(
							s"${read.getReadName}/$readNameSfx",
							seqId,
							pos,
							posInRead,
							alleleRead,
							read.getUnclippedStart,
							read.getMappingQuality,
							read.getCigarString,
							read.getReadNegativeStrandFlag,
							if (mateRefName.isDefined) mateRefName.get else "NA",
							read.getMateAlignmentStart,
							read.getInferredInsertSize,
							! read.getMateUnmappedFlag,
							read.getProperPairFlag)
				}
			}
			readsIter.close
			alignments.toList
		}

		def extractOrigins(reader: SamReader, readNames: Set[String]) : Map[String, SAMRecord] = {
			val origins = new ListBuffer[(String, SAMRecord)]
			val iter = reader.iterator
			while (iter.hasNext) {
				val read = iter.next
				val readNameSfx = if (read.getFirstOfPairFlag) "1" else "2"
				val readName = s"${read.getReadName}/$readNameSfx"
				if (readNames.contains(readName)) {
					origins += (readName -> read)
				}
			}
			iter.close
			return origins.toMap
			// HashMap[String,ReadOrigin]() ++ origins.map(x => (x.readName -> x))
		}
	}

	object VariantFactory {
		def fromString(row: String) : Variant = {
			val fields = row.split("\t").toList
			val seqId :: posStr :: dbid :: alleleRef :: alleleVar :: qualStr :: filt :: xs = fields
			Variant(seqId, posStr.toInt, alleleRef, alleleVar, filt)
		}
	}

	case class Variant(
			seqId: String, 
			pos: Int, 
			alleleRef: String, 
			alleleVar: String, 
			filt: String) {
		def asString: String = {
			List(seqId, pos.toString, alleleRef, alleleVar).mkString("\t")
		}
	}

	case class ReadAlignment(
			readName: String,
			seqId: String,
			pos: Int,
			posInRead: Int,
			alleleRead: String,
			alnStart: Int,
			mq: Int,
			cigar: String,
			isNegStrand: Boolean,
			mateSeqId: String,
			mateAlnStart: Int,
			insertSize: Int,
			mateMapped: Boolean,
			properPair: Boolean) {

		def asString: String = {
			List(
				readName, 
				posInRead.toString, 
				alleleRead, 
				alnStart.toString, 
				mq.toString,
				cigar,
				if (isNegStrand) "-" else "+",
				mateSeqId,
				mateAlnStart.toString,
				insertSize.toString,
				if (mateMapped) "1" else "0",
				if (properPair) "1" else "0"
			).mkString("\t")
		}
	}


	object ReadOriginFactory {
		def mkEmpty : ReadOrigin = {
			ReadOrigin("", "", 0, 0, "")
		}
	}

	case class ReadOrigin(
			readName: String,
			seqId: String,
			readStart: Int, 
			insertSize: Int,
			cigar: String) {

		def asString: String = {
			List(
				seqId, 
				readStart.toString, 
				insertSize.toString,
				cigar
			).mkString("\t")
		}
	}

}
