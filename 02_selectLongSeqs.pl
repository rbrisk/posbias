#!/usr/bin/env perl

use strict;
use Carp;
use Data::Dumper;
use Getopt::Long;
use Log::Log4perl qw(:easy);
use Pod::Usage;
use Bio::SeqIO;


our $delim = "\t";
my $man = 0;
my $help = 0;
my ($pathIn, $pathOut, $lenThres, $lineWidth);

GetOptions(
		"h|help" => \$help,
		"man"    => \$man,
		"i:s"    => \$pathIn,
		"o:s"    => \$pathOut,
		"t=i"    => \$lenThres,
		"w=i"    => \$lineWidth,
) or pod2usage(2);

pod2usage(1) if $help;
pod2usage(-verbose => 2) if $man;


open(my $fhIn,  '<' . ($pathIn  || '-')) or confess("Unable to read the input from '$pathIn'. $!");
open(my $fhOut, '>' . ($pathOut || '-')) or confess("Unable to write the output to '$pathOut'. $!");

my $reader = Bio::SeqIO->new(-fh => $fhIn,  -format => 'fasta', -alphabet => 'dna');
my $writer = Bio::SeqIO->new(-fh => $fhOut, -format => 'fasta', -alphabet => 'dna');
$writer->width($lineWidth) if $lineWidth;

my $totalLen = 0;
my $filtLen = 0;
my $totalSeqN = 0;
my $filtSeqN = 0;
while (my $seq = $reader->next_seq()) {
	$totalLen += $seq->length;
	++$totalSeqN;
	if ($seq->length >= $lenThres) {
		$writer->write_seq($seq);
		$filtLen += $seq->length;
		++$filtSeqN;
	}
}
$writer->close();
$reader->close();

print STDERR "$filtSeqN out of $totalSeqN sequences satisfy $lenThres bp threshold\n";
print STDERR "Assembly size reduced from $totalLen to $filtLen\n";



__END__

=encoding utf8

=head1 NAME

02_selectLongSeqs.pl - selects sequences longer than the specified threshold.

=head1 SYNOPSIS

02_selectLongSeqs.pl -i input.fasta -o output.fasta -t 200

=head1 OPTIONS

=over 8

=item B<-t>

Minimum length of a sequence in bp.

=item B<-i>

(Optional) Input file in fasta format. Default: STDIN

=item B<-o>

(Optional) Output file in fasta format. Default: STDOUT

=item B<-w>

(Optional) Line width for the output file.

=back

=head1 DESCRIPTION

Selects sequences that are longer than the specified threshold and prints them to the specified file
or STDOUT.

=head1 AUTHOR

Roman Briskine, Universität Zürich

=cut

