#!/usr/bin/env perl

use strict;
use warnings;
use Getopt::Long;
use Pod::Usage;

use Bio::SeqIO;


my $man = 0;
my $help = 0;
my $minLen = 500;
my ($pathFasta, $pathOut, $pos);

GetOptions(
		"h|help" => \$help,
		"man"    => \$man,
		"i=s"    => \$pathFasta,
		"o=s"    => \$pathOut,
		"pos=i"    => \$pos,
		"minLen=i" => \$minLen,
	) or pod2usage(2);

pod2usage(1) if $help;
pod2usage(-verbose => 2) if $man;

die("Input file is required") unless $pathFasta;
die("Position is required") unless $pos;
die("Position '$pos' should be positive") unless $pos > 0;
die("Position '$pos' should be smaller than minimum length '$minLen'") unless $pos < $minLen;

my $rowTemplate = join("\t", '%1$s', '%2$d', '.', '%3$s', '%3$s', "", "PASS", ("") x 3) . "\n";

my $reader = Bio::SeqIO->new(-file => $pathFasta, -format => 'fasta', -alphabet => 'dna');
my ($cnt, $cntOut) = (0, 0);
open(FOUT, "> " . ($pathOut || '-')) or die("Unable to write the output ($pathOut). $!");
while (my $seq = $reader->next_seq()) {
	++$cnt;
	next unless $seq->length >= $minLen;
	++$cntOut;
	my $seqId = $seq->display_id;
	my $ref = $seq->subseq($pos, $pos);
	printf FOUT $rowTemplate, $seqId, $pos, $ref;
	my $pos3p = $seq->length - $pos + 1;
	$ref = $seq->subseq($pos3p, $pos3p);
	printf FOUT $rowTemplate, $seqId, $pos3p, $ref;
}
$reader->close;
close(FOUT);

print STDERR "Selected loci from $cntOut out of $cnt sequences\n";

__END__

=encoding utf8

=head1 NAME

07_selectPos.pl - Emulates variant file for ExtractReadInfo script.

=head1 SYNOPSIS

07_selectPos.pl -i input.fasta -o variants.txt -p 72

=head1 OPTIONS

=over 8

=item B<-i>

Input file in FASTA format.

=item B<-o>

(Optional) Output file. It has the same format as VCF but does not contain any headers. Default: 
STDOUT

=item B<--pos>

Position (both ends of each sequence are checked).

=item B<--minLen>

(Optional) Minimum length of the sequence. Shorter sequences are ignored. Default: 500

=back

=head1 DESCRIPTION

The script creates a fake VCF file that contains all nucleotides at the specified position from each end. The file can be used as input for ExtractReadInfo script, so that it could retrieve information about all reads aligning to the specified position within the assembly's sequences. The format of the output file is similar to VCF. However, it lacks headers and provides empty values for fields that are not used by ExtractReadInfo.

=head1 AUTHOR

Roman Briskine, Universität Zürich

=cut

