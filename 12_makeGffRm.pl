#!/usr/bin/env perl

use strict;
use warnings;
use Bio::SeqFeature::Generic;
use Bio::Tools::GFF;
use Getopt::Long;
use Pod::Usage;

our $delim = "\t";
my $help = 0;
my $man = 0;
my $sourceTag = "RepeatMasker";
my $toAbsolute = 0;
my ($pathIn, $pathOut);

GetOptions(
		"h|help" => \$help,
		"man"    => \$man,
		"i=s"    => \$pathIn,
		"s=s"    => \$sourceTag,
		"o=s"    => \$pathOut,
		"a"      => \$toAbsolute,
	) or pod2usage(2);

pod2usage(1) if $help;
pod2usage(-verbose => 2) if $man;

die("Input file is required") unless $pathIn;
die("Output file is required") unless $pathOut;
die("Input file ($pathIn) does not exist") unless -f $pathIn;

my $writer = Bio::Tools::GFF->new(-file => "> $pathOut", -gff_version => 3);
open(FIN, "<", $pathIn) or die("Unable to read the input file ($pathIn). $!");

# Skip first three lines
for (my $i = 0; $i < 3; $i++) {
	<FIN>;
}

while (<FIN>) {
	chomp;
	# remove leading spaces
	s/^\s+//;
	my @fields = split(/\s+/);
	my $seqId = $fields[4];
	my ($queryBeg, $queryEnd);
	if ($toAbsolute) {
		($seqId, $queryBeg, $queryEnd) = split(/_/, $fields[4]);
	}

	my $tags = {
			Name    => $fields[ 9],
			family  => $fields[10],
			pcntDiv => $fields[ 1],
			pcntDel => $fields[ 2],
			pcntIns => $fields[ 3] };
	
	my ($start, $end) = @fields[5,6];
	
	if ($toAbsolute) {
		$start += $queryBeg - 1;
		$end   += $queryBeg - 1;
	}

	my $f = Bio::SeqFeature::Generic->new(
			-seq_id     => $seqId,
			-source_tag => $sourceTag,
			-primary_tag=> "transposon",
			-start      => $start,
			-end        => $end,
			-score      => $fields[0],
			-strand     => $fields[8] eq "+" ? 1 : -1,
			-frame      => ".",
			-tag        => $tags );

	$writer->write_feature($f);
}

close(FIN);
$writer->close();


__END__

=head1 NAME

12_makeGffRm.pl - Makes GFF v3 file out of the RepeatMasker output (.out extension).

=head1 SYNOPSIS

12_makeGffRm.pl -i locus.fa.out -s RepeatMasker -o locus.gff

=head1 OPTIONS

=over 8

=item B<-i>

Input file generated by RepeatMasker with .out extension.

=item B<-o>

Output GFF v3 file.

=item B<-s>

(Optional) Source tag to use when some other program produced RepeatMasker formatted output.
Default: RepeatMasker

=item B<-a>

(Optional) If specified, converts relative coordinates into absolute. The source is expected to have
"Chr?_Beg_End" format, e.g. "Chr3_150380_160379".

=back

=head1 DESCRIPTION

The script generates GFF v3 file out of the RepeatMasker output file (.out file rather than the
optional GFF v2 file that lacks a lot of information). If RepeatMasker was run on a small locus, the
script can also convert relative coordinates of the hits into absolute coordinates. In that case,
the script expects that the query name follows the "Chr?_Beg_End" format, e.g. "Chr3_150380_160379".

=head1 AUTHOR

Roman Briskine, Universität Zürich

=cut


