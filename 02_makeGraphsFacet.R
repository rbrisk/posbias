#!/usr/bin/env Rscript

require(optparse, quietly=T)
require(dplyr, quietly=T, warn.conflicts=F)
require(futile.logger, quietly=T)
require(grid)
require(ggplot2, quietly=T)
require(readr, quietly=T)
require(stringr, quietly=T)

optList <- list(
		make_option(c("-i", "--variants"), type = "character",
				help = "Path to the tidied variant table"),
		make_option(c("-o", "--dirOut"), type = "character",
				help = "Output directory"),
		make_option(c("-t", "--selectType"), type = "character", default = "ALL",
				help = "Variant type filter ('SNP', 'INDEL', or 'ALL'). Default: \"%default\""),
		make_option("--thresLowCov", type = "integer", default = 75,
				help = "Coverage threshold for zoomed in coverage plot. Default: %default"),
		make_option("--thresLowFS", type = "integer", default = 20,
				help = "Fisher strand threshold for zoomed in FS plot. Default: %default"),
		make_option("--posRangeStr", type="character", default = 'c(0, 150)',
				help = "Position range for variant count histograms. Default: '%default'"),
		make_option("--minSeqLen", type = "integer", default = 1,
				help = "Minimum sequence length. Variants on shorter sequences are ignored. Default: %default"),
		make_option("--binWidth", type = "integer", default = 1,
				help = "Bin width for variant count histograms. Default: %default"),
		make_option("--imgThemeStr", type = "character", default = "theme_black",
				help = "ggplot2 theme string. Default: \"%default\""),
		make_option("--histFill", type = "character", default = "#333333",
				help = "Histogram fill colour. Default: \"%default\""),
		make_option("--paletteStr", type = "character", default = 'c("#E69F00", "#56B4E9")',
				help = "Colour palette for density plots. Default: '%default'"),
		make_option("--alpha", type = "double", default = 0.5,
				help = "Colour opacity for density plots. Default: %default"),
		make_option("--ncol", type = "integer", default = 2,
				help = "Number of panel columns. Default: %default"),
		make_option("--imgType", type = "character", default = "svg",
				help = "Image type ('svg', 'png', 'jpg', 'tiff', 'pdf'). Default: \"%default\""),
		make_option("--imgPrefix", type = "character", default = "",
				help = "Prefix for image file names. Default: \"%default\""),
		make_option("--imgHeight", type = "double", default = 12,
				help = "Image height in cm. Default: %default"),
		make_option("--imgWidth", type = "double", default = 12,
				help = "Image width in cm. Default: %default"),
		make_option("--panelOrder", type = "character",
				help = "(Optional) Panel order (comma-separated list of integers to reorder the RefName factor)."),
		make_option(c("-l", "--verbosity"), type = "integer", default = INFO,
				help = "Logging verbosity (t=9,d=8,i=6,w=4,e=2,f=1). Default: %default")
	)


opt <- parse_args( OptionParser(
		usage = "Usage: %prog [options]",
		option_list = optList,
		description = "Uses facets to generates variant count histograms and various variant density plots") )

allImgThemes = c("theme_grey", "theme_gray", "theme_bw", "theme_black", "theme_classic", 
		"theme_minimal", "theme_light", "theme_linedraw")
allImgTypes = c("svg", "png", "jpg", "tiff", "pdf")

devnull <- flog.threshold(opt$verbosity)

opt$posRange = eval(parse(text=opt$posRangeStr))
opt$palette = eval(parse(text=opt$paletteStr))

if (any(allImgThemes == opt$imgThemeStr)) {
	opt$imgTheme <- eval(parse(text=sprintf("%s()", opt$imgThemeStr)))
} else {
	stop("Invalid ggplot theme")
}

if ( ! any(allImgTypes == opt$imgType) ) {
	stop("Invalid image type")
}


if ( ! file.exists(opt$dirOut) ) {
	dir.create(opt$dirOut, recursive=T);
}

if (nchar(opt$imgPrefix) > 0) {
	opt$imgPrefix <- sprintf("%s_", opt$imgPrefix)
}
	

flog.info("Loading the data")
# If contigs only have numeric part, we have to convert Int to Char just in case.
# Also, I change the binary Is.k to boolean or else I will forget to factor it
# It is safer to specify column types because readr cannot determine the correct type for CHROM as
# some data sets have numeric ids while others are strings.
variants <- read_tsv(opt$variants, col_types="cicdddddddiiciiii") %>%
		mutate(Is.k = as.logical(Is.k))
flog.debug("Found %d records", nrow(variants))

# Select requested type unless ALL types are requested
if (opt$selectType != "ALL") {
	flog.info("Filtering the data by type")
	variants <- filter(variants, TYPE == opt$selectType)
	flog.debug("After type filter: %d", nrow(variants))
}

# Remove SNPs on contigs that are too small
if (opt$minSeqLen > 1) {
	variants <- filter(variants, SeqLength >= opt$minSeqLen)
	flog.debug("After sequence length filter: %d", nrow(variants))
}

if (nrow(variants) == 0) {
	stop(sprintf("The input table has no records of type %s", opt$selectType)) 
}

# Making RefName a factor will allow us to apply custom ordering
variants$RefName <- factor(variants$RefName)
# Set panel order if necessary
if (!is.null(opt$panelOrder)) {
	panelOrderArr <- as.integer(unlist(str_split(opt$panelOrder, ",")))
	refNameLevels <- levels(variants$RefName)
	panelN <- length(refNameLevels)
	# Make sure the vector is correct
	if (any(sort(panelOrderArr) != 1:panelN)) {
		stop(sprintf("The panel order array should have each number in 1:%d exactly once.", panelN))
	}
	variants$RefName <- factor(variants$RefName, refNameLevels[panelOrderArr])
}

flog.info("Rendering graphs")
# 5' end
# The X scale is continuous to avoid vertical lines between bars
pathPos5p <- file.path(opt$dirOut, sprintf("%sPos5p.%s", opt$imgPrefix, opt$imgType))
flog.debug("Selecting variants at the 5' end")
variants5p <- filter(variants, POS >= opt$posRange[1], POS <= opt$posRange[2])
flog.debug("Variants 5' graph")
gObj <- ggplot(variants5p, aes(POS)) + 
		opt$imgTheme + 
		geom_histogram(binwidth=opt$binWidth, fill=opt$histFill) +
		scale_x_continuous(name="Position 5'", limits=opt$posRange, expand=c(0,0)) +
		ylab("Count") +
		facet_wrap(~RefName, ncol=opt$ncol, scales="free")
ggsave(pathPos5p, gObj, width=opt$imgWidth, height=opt$imgHeight, units="cm")

# 3' end
pathPos3p <- file.path(opt$dirOut, sprintf("%sPos3p.%s", opt$imgPrefix, opt$imgType))
flog.debug("Selecting variants at the 3' end")
variants3p <- filter(variants, Pos3p >= opt$posRange[1], Pos3p <= opt$posRange[2])
flog.debug("Variants 3' graph")
gObj <- ggplot(variants3p, aes(Pos3p)) + 
		opt$imgTheme + 
		geom_histogram(binwidth=opt$binWidth, fill=opt$histFill) +
		scale_x_continuous(name="Position 3'", limits=opt$posRange, expand=c(0,0)) +
		ylab("Count") +
		facet_wrap(~RefName, ncol=opt$ncol, scales="free")
ggsave(pathPos3p, gObj, width=opt$imgWidth, height=opt$imgHeight, units="cm")

renderDensity <- function(df, colName, fnSuffix, labelX) {
	pathOut <- file.path(opt$dirOut, sprintf("%s%s.%s", opt$imgPrefix, fnSuffix, opt$imgType))
	gObj <- ggplot(df, aes_string(x=colName)) +
			opt$imgTheme +
			stat_density(aes(fill=Is.k), linetype=0, alpha=opt$alpha, position="identity") +
			xlab(labelX) +
			scale_fill_manual(
					values=opt$palette, 
					breaks=c(T,F), 
					labels=c("Yes", "No"), 
					guide=guide_legend(title="Pos k")) +
			facet_wrap(~RefName, ncol=opt$ncol, scales="free")
	ggsave(pathOut, gObj, width=opt$imgWidth, height=opt$imgHeight, units="cm")
}

variants <- mutate(variants, Cov = CovRef + CovAlt) %>% mutate(VarFreq = CovAlt / Cov)
flog.debug("QD graph")
renderDensity(variants, "QD", "QD", "Quality by Depth")
flog.debug("Cov graph")
renderDensity(variants, "Cov", "Cov", "Coverage")
flog.debug("Cov zoom graph")
renderDensity(filter(variants, Cov <= opt$thresLowCov), "Cov", "CovLow", "Coverage")
flog.debug("VarFreq graph")
renderDensity(variants, "VarFreq", "VarFreq", "Variant Frequency")
flog.debug("MQ graph")
renderDensity(variants, "MQ", "MQ", "Mapping Quality")
flog.debug("MQRankSum graph")
renderDensity(filter(variants, !is.na(MQRankSum)), "MQRankSum", "MQRankSum", "MQ Rank Sum")
flog.debug("FS graph")
renderDensity(variants, "FS", "FS", "Fisher Strand Bias")
flog.debug("FS zoom graph")
renderDensity(filter(variants, FS <= opt$thresLowFS), "FS", "FSLow", "Fisher Strand Bias")
flog.debug("SOR graph")
renderDensity(variants, "SOR", "SOR", "Strand Odds Ratio")
flog.debug("ReadPosRankSum graph")
renderDensity(filter(variants, !is.na(ReadPosRankSum)), "ReadPosRankSum", "ReadPosRankSum", 
		"Read Pos Rank Sum")
flog.debug("GQ graph")
renderDensity(variants, "GQ", "GQ", "Genotype Quality")

flog.info("Done")
