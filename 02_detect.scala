package ch.uzh.shimizulab.posbias

import org.broadinstitute.gatk.queue.QScript
import org.broadinstitute.gatk.queue.extensions.gatk._
import org.broadinstitute.gatk.queue.extensions.picard._
import org.broadinstitute.gatk.queue.function.JavaCommandLineFunction
import org.broadinstitute.gatk.queue.util.Logging

import htsjdk.samtools.ValidationStringency
import htsjdk.samtools.SAMFileHeader.SortOrder

import scala.util.matching._


class DetectPosBias extends QScript with Logging {

	qs =>

	@Input(doc="The reference file in FASTA format", 
			shortName="R", fullName="reference", required=true)
	var pathRef: File = _

	@Input(doc="File with reads. May be archived with gzip", 
			shortName="reads", fullName="reads", required=true)
	var pathReads: File = _

	@Input(doc="File with mates. May be archived with gzip",
			shortName="mates", fullName="mates", required=false)
	var pathMates: Option[File] = None

	@Argument(doc="Number of data threads to use for jobs that support -nt. This does not apply to " +
			"jobs that support scattering.", shortName="t", fullName="threadN", required=false)
	var threadN: Int = 8

	@Argument(doc="Number of CPU threads to use for jobs that support -nct. Scatter jobs will get " +
			"ct / sj threads each", shortName="ct", fullName="cpuThreadN", required=false)
	var cpuThreadN: Int = 8

	@Argument(doc="Number of scatter jobs to generate", 
			shortName="sj", fullName="scatterN", required=false)
	var scatterN: Int = 8

	@Argument(doc="Maximum amount of memory to use in GB",
			shortName="maxMem", fullName="maxMemory", required=false)
	var maxMem: Int = 32

	@Argument(doc="Maximum records in RAM", 
			shortName="maxRec", fullName="maxRecordsInRam", required=false)
	var maxRecordsInRam: Int = 1e+7.toInt

	@Argument(doc="Maximum file handles for MarkDuplicates",
			shortName="maxFh", fullName="maxFileHandleN", required=false)
	var maxFileHandleN: Int = 300

	@Argument(doc="Optional list of filter names", 
			fullName="filterName", shortName="filterName", required=false)
	var filterNames: List[String] = Nil

	@Argument(doc="Optional list of filter expressions", 
			shortName="filter", fullName="filter", required=false)
	var filterExpressions: List[String] = Nil

	@Argument(doc="Optional list of genotype filter names", fullName="gFilterName",
			shortName="gFilterName", required=false)
	var gFilterNames: List[String] = Nil

	@Argument(doc="Optional list of genotype filer expressions", fullName="gFilterExpression",
			shortName="gFilter", required=false)
	var gFilterExpressions: List[String] = Nil

	@Argument(doc="Read group",
			shortName="rg", fullName="readGroup", required=false)
	var readGroup: Option[String] = None

	@Argument(doc = "Seed for pseudo-random number generation in bowtie2",
			shortName = "seed", fullName = "seed", required = false)
	var seed: Int = 0

	@Argument(doc = "Deprecated in favour of --aligner.", required = false,
			shortName = "bt", fullName = "bowtie2")
	var useBowtie2: Boolean = false

   @Argument(doc = "Aligner: bwa, bowtie2, ngm, gsnap", required = false,
			shortName = "aln", fullName = "aligner")
	var aligner: String = "bwa"

	@Argument(doc = "GSNAP database directory", fullName = "gsnapD", required = false)
	var gsnapD: String = _

	@Argument(doc = "Filter reads with Ns in CIGAR. May be necessary for GMAP.", required = false,
			fullName = "filterNCIGAR")
	var filterNCigar: Boolean = false

	trait commonArgs extends CommandLineGATK {
		this.reference_sequence = qs.pathRef
	}

	trait commonAlignerArgs extends Aligner {
		this.pathRef = qs.pathRef
		this.pathReads = qs.pathReads
		this.pathMates = qs.pathMates
		this.readGroup = qs.readGroup
		this.memoryLimit = qs.maxMem
		this.nCoresRequest = qs.threadN
		this.pathOut = "aligned.bam"
		this.isIntermediate = true
	}


	def script() {
		// Align the reads to the reference
		if (qs.useBowtie2) {
			qs.aligner = "bowtie2"
		}
		var aligner = qs.aligner.toLowerCase match {
			case "bwa" => 
					new BwaAligner with commonAlignerArgs {
						this.analysisName = "BwaAligner"
					}
			case "bowtie2" =>
					new Bowtie2Aligner with commonAlignerArgs {
						this.analysisName = "Bowtie2"
						this.seed = qs.seed
					}
			case "ngm" =>
					new NextGenMapAligner with commonAlignerArgs {
						this.analysisName = "NextGenMap"
					}
			case "gsnap" =>
					new GsnapAligner with commonAlignerArgs {
						this.analysisName = "GSNAP"
						this.gsnapd = qs.pathRef.getName.replaceFirst("""\.[^.]+$""", "")
						this.gsnapD = qs.gsnapD.getAbsolutePath
					}
		}

		// Fix mates
		val fixMates = new FixMateInformation {
			this.isIntermediate = true
			this.input = "aligned.bam"
			this.out = "matesFixed.bam"
			this.sortOrder = SortOrder.coordinate
			this.createIndex = Some(true)
			this.validationStringency = ValidationStringency.LENIENT
			this.maxRecordsInRam = qs.maxRecordsInRam
			this.javaMemoryLimit = qs.maxMem
		}

		// Mark duplicates
		val markDuplicates = new MarkDuplicates {
			this.isIntermediate = false
			this.input :+= fixMates.out
			this.output = "duplicatesMarked.bam"
			// Previously, it tried to get output value before it was frozen. 
			// Not sure if it is still true.
			this.metrics = "duplicatesMarked.metrics"
			this.REMOVE_DUPLICATES = true
			this.MAX_FILE_HANDLES_FOR_READ_ENDS_MAP = qs.maxFileHandleN
			this.javaMemoryLimit = qs.maxMem
		}

		// Create target intervals and realign
		val realignerTargetCreator = new RealignerTargetCreator with commonArgs {
			this.isIntermediate = true
			// It does not parallelise very well. Most of the time, it uses 2-4 threads.
			this.num_threads = Math.max(qs.threadN, 4)
			this.memoryLimit = qs.maxMem
			this.input_file :+= markDuplicates.output
			this.filter_reads_with_N_cigar = qs.filterNCigar
			this.out = "realigned.intervals"
		}
		val realigner = new IndelRealigner with commonArgs {
			this.isIntermediate = true
			this.scatterCount = qs.scatterN
			this.num_cpu_threads_per_data_thread = 1
			this.num_threads = 1
			this.memoryLimit = (qs.maxMem / qs.threadN).toInt
			this.input_file :+= markDuplicates.output
			this.filter_reads_with_N_cigar = qs.filterNCigar
			this.targetIntervals = realignerTargetCreator.out
			this.out = "realigned.bam"
		}

		// Call variants
		val haplotypeCaller = new HaplotypeCaller with commonArgs {
			this.isIntermediate = false
			this.scatterCount = qs.scatterN
			val adjCpuThreadN = (qs.cpuThreadN / qs.scatterN).toInt
			this.num_cpu_threads_per_data_thread = if (adjCpuThreadN > 1) adjCpuThreadN else 1
			this.memoryLimit = (qs.maxMem / qs.threadN).toInt
			this.input_file :+= realigner.out
			this.out = "raw.vcf.gz"
		}

		// Evaluate unfiltered variants
		val evalUnfiltered = new VariantEval with commonArgs {
			this.isIntermediate = false
			// Even though it is implemented, it still uses only a single thread ???
			// this.num_threads = qs.threadN
			this.memoryLimit = qs.maxMem
			this.eval :+= haplotypeCaller.out
			this.out = "raw_report.txt"
		}

		// Filter variants
		val variantFilter = new VariantFiltration with commonArgs {
			this.isIntermediate = false
			// Sometimes it gets very hungry for memory, e.g. 32 GB may not be enough
			this.memoryLimit = qs.maxMem
			this.variant = haplotypeCaller.out
			this.out = "filtered.vcf.gz"
			this.filterName = qs.filterNames
			this.filterExpression = qs.filterExpressions
			this.genotypeFilterName = qs.gFilterNames
			this.genotypeFilterExpression = qs.gFilterExpressions
		}

		// Evaluate filtered variants
		val evalFiltered = new VariantEval with commonArgs {
			this.isIntermediate = false
			// Even though it is implemented, it still uses only a single thread ???
			// this.num_threads = qs.threadN
			this.memoryLimit = qs.maxMem
			this.eval :+= variantFilter.out
			this.out = "filtered_report.txt"
		}

		// Convert to table
		val variantsToTable = new VariantsToTable with commonArgs {
			this.isIntermediate = false
			this.fields ++= List("CHROM", "POS", "TYPE", "QUAL", "QD", "MQ", "FS", "SOR", "MQRankSum", 
					"ReadPosRankSum")
			this.genotypeFields ++= List("AD", "GQ")
			this.allowMissingData = true
		}
		if (filterNames.size > 0) {
			variantsToTable.out = "filtered.txt"
			variantsToTable.variant :+= variantFilter.out
		} else {
			variantsToTable.out = "raw.txt"
			variantsToTable.variant :+= haplotypeCaller.out
		}

		// Add evalUnfiltered back after the initial run. It is very slow.
		add(
				aligner, 
				fixMates,
				markDuplicates,
				realignerTargetCreator,
				realigner,
				haplotypeCaller
			)
		// Add evalFiltered back after the initial run. It is very slow.
		if (filterNames.size > 0) {
			add(variantFilter)
		}
		add(variantsToTable)
	}


	abstract class Aligner extends CommandLineFunction {
		@Input(doc="Indexed reference file in FASTA format")
		var pathRef: File = _

		@Input(doc="List of files with first mates (may be compressed)")
		var pathReads: File = _

		@Input(doc="List of file with second mates (may be compressed). Must be in the same order " +
				"as first mates", required=false)
		var pathMates: Option[File] = _

		@Output(doc="Output in bam format sorted by query")
		var pathOut: File = _

		@Argument(doc="Read group information", required=false)
		var readGroup: Option[String] = None
	}

	class BwaAligner extends Aligner {
		// -M is required for picard compatibility. -v 2 in principle should reduce verbosity.
		def commandLine = "bwa mem " + 
				"-M -v 2" +
				required("-t", nCoresRequest, escape=false) + 
				optional("-R", readGroup) +
				required(pathRef) +
				required(pathReads) +
				optional(pathMates) +
				"| samtools sort -T tmpSort -n " +
				optional("-@", nCoresRequest, escape=false) +
				optional("-m", memoryLimit.toInt + "G", escape=false) +
				required("-o", pathOut)
	}


	class Bowtie2Aligner extends Aligner {
		@Argument(doc = "Seed for pseudo-random number generation", required = false)
		var seed: Int = 0

		def commandLine = {
			// Split at tabs and drop @RG
         // Note that the string contains \\t rather than tabs
			val rgTags = readGroup.flatMap((x: String) => Some(x.split("""\\t""").tail))
			// Get the ID string from the first element
			val rgId = rgTags.flatMap((x: Array[String]) => Some(x.head.split(":").last))
			// Drop the ID tag from the array
			val rgOther = rgTags.flatMap((x: Array[String]) => Some(x.tail.toList))
			"bowtie2 -q " + 
			optional("--rg-id", rgId) +
			repeat("--rg", rgOther.getOrElse(List[String]())) +
			required("-p", nCoresRequest, escape=false) + 
			required("--seed", seed) +
			required("-x", pathRef) +
			required("-1", pathReads) +
			required("-2", pathMates) +
			"| samtools sort -T tmpSort -n " +
			optional("-@", nCoresRequest, escape=false) +
			optional("-m", memoryLimit.toInt + "G", escape=false) +
			required("-o", pathOut)
      }
	}


	class NextGenMapAligner extends Aligner {
		def commandLine = {
			// Split by tab character, remove the @RG prefix, replace colon with a space and change the
			// field name to --rg-xx
			val rgParams = readGroup.flatMap(
				(x:String) => Some(x.split("""\\t""").tail.map(
					_.split(":") match { 
						case Array(a, b) => s"--rg-${a.toLowerCase} $b" 
					}
				))
			)
			
			"ngm " + 
					required("-r", pathRef) +
					required("-1", pathReads) +
					optional("-2", pathMates) +
					rgParams.getOrElse(Array("")).mkString(" ") +
					required("-t", nCoresRequest, escape=false) + 
					"| samtools sort -T tmpSort -n " +
					optional("-@", nCoresRequest, escape=false) +
					optional("-m", memoryLimit.toInt + "G", escape=false) +
					required("-o", pathOut)
		}
	}


	class GsnapAligner extends Aligner {
		@Argument(doc = "Index name", required = true)
		var gsnapd: String = _

		@Argument(doc = "Index output directory", required = true)
		var gsnapD: String = _

		def rgFieldToOption(s: String) = {
			val p = "--read-group"
			s.split(":") match {
				case Array("ID", a) => s"${p}-id $a"
				case Array("SM", a) => s"${p}-name $a"
				case Array("LB", a) => s"${p}-library $a"
				case Array("PL", a) => s"${p}-platform $a"
				case _ => ""
			}
		}

		def commandLine = {

			// Split by tab character, remove the @RG prefix, replace colon with a space and change the
			// field name to --read-group-xxxxx
			val rgParams = readGroup.flatMap(
				(x:String) => Some(x.split("""\\t""").tail.map(rgFieldToOption(_)))
			)
			
			"gmap_build " +
					required("-d", gsnapd) +
					required("-D", gsnapD) +
					required(pathRef) +
					" && " +
					"gsnap -A sam --gunzip " + 
					required("-d", gsnapd) +
					required("-D", gsnapD) +
					required("-t", nCoresRequest, escape=false) + 
					rgParams.getOrElse(Array("")).mkString(" ") +
					required(pathReads) +
					optional(pathMates) +
					"| samtools sort -T tmpSort -n " +
					optional("-@", nCoresRequest, escape=false) +
					optional("-m", memoryLimit.toInt + "G", escape=false) +
					required("-o", pathOut)
		}
	}


	class FixMateInformation extends JavaCommandLineFunction with PicardBamFunction  {
		analysisName = "FixMateInformation"
		javaMainClass = "picard.sam.FixMateInformation"

		@Input(doc = "The input SAM or BAM files to analyze. Must be coordinate sorted.", 
				required = true)
		var input: File = _

		@Output(doc="The output file to write fixed records to", required = true)
		var out: File = _

		@Output(doc="The output bam index", required = false)
		var outIndex: File = _

		@Argument(doc="If true, Adds the mate CIGAR tag (MC)", required = false)
		var addMateCigar: Boolean = false

		override def freezeFieldValues() {
			super.freezeFieldValues()
			if (outIndex == null && out != null) {
				outIndex = new File(out.getName.stripSuffix(".bam") + ".bai")
			}
		}

		override def inputBams = Seq(input)
		override def outputBam = out
		this.createIndex = Some(true)

		override def commandLine = super.commandLine +
				conditional(addMateCigar, "ADD_MATE_CIGAR=true")
	}

}

