#!/usr/bin/env perl

use strict;
use warnings;
use Bio::SeqIO;
use Getopt::Long;
use Pod::Usage;

my $man = 0;
my $help = 0;
my $delim = "\t";
my $ptrn = qr/^(.+)$/;
my ($pathIn, $pathOut);

GetOptions(
      "h|help" => \$help,
      "man"    => \$man,
      "i=s"    => \$pathIn,
      "o=s"    => \$pathOut,
		"p=s"    => \$ptrn,
   ) or pod2usage(2);

pod2usage(1) if $help;
pod2usage(-verbose => 2) if $man;

open(my $fhIn,  "< " . ($pathIn  || '-')) or die("Unable to read the input from '$pathIn'. $!");
open(my $fhOut, "> " . ($pathOut || '-')) or die("Unable to write the output to '$pathOut'. $!");
print $fhOut join($delim, qw/ OrigId NewId /), "\n";

my $reader = Bio::SeqIO->new(-fh => $fhIn, -format => 'fasta', -alphabet => 'dna');
while (my $seq = $reader->next_seq()) {
	# We assume that the original id is the first word in the description
	(my $origId = $seq->desc) =~ s/^(\S+).*$/$1/;
	$origId =~ s/$ptrn/$1/;
	print $fhOut join($delim, $origId, $seq->display_id), "\n";
}

$fhIn->close();
$fhOut->close();

__END__

=encoding utf8

=head1 NAME

05_makeContigNameMap.pl - Creates a map of old contig ids to new contig ids.

=head1 SYNOPSIS

05_makeContigNameMap.pl -i input.fa -o map.txt

=head1 OPTIONS

=over 8

=item B<-i>

(Optional) Input file processed to have sequential unique contig ids while retaining old contig ids
as the first word in the description. Default: STDIN

=item B<-o>

(Optional) Output file that contains two tab-delimited columns: OrigId and NewId. Default: STDOUT

=item B<-p>

(Optional) Substitution pattern to apply to the original contig ids. Default: qr/^(.+)$/
=back

=head1 DESCRIPTION

The script builds a map from the original contig ids to the new contig ids using fasta file as input. The script expects that the first word in each record description is the original id. The output is a tab-delimited file that contains two columns: OrigId and NewId.

=head1 AUTHOR

Roman Briskine, Universität Zürich

=cut

